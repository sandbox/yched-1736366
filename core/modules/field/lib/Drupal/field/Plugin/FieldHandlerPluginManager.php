<?php

/**
 * @file
 *
 * Definition of Drupal\field\Plugin\FieldHandlerPluginManager.
 */

namespace Drupal\field\Plugin;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Plugin\Discovery\CacheDecorator;
use Drupal\Core\Plugin\Discovery\HookDiscovery;
use Drupal\Component\Plugin\Factory\ReflectionFactory;

/**
 * 'Field type handler' plugin type manager.
 */
class FieldHandlerPluginManager extends PluginManagerBase {

  protected $defaults = array(
    'settings' => array(),
    'instance_settings' => array(),
  );

  protected $cache_bin = 'field';
  protected $cache_key = 'field_types';
  protected $hook = 'field_info2';

  public function __construct() {
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    $this->factory = new ReflectionFactory($this);
  }

  /**
   * Clear cached definitions.
   * @todo A cleaner way should be baked within CacheDecorator.
   */
  public function clearDefinitions() {
    // Clear 'static' data by creating a new object.
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    cache($this->cache_bin)->delete($this->cache_key);
  }

  /**
   * Overrides PluginManagerBase::getInstance().
   * @todo This takes the place of a Mapper - Do we want a real Mapper instead ?
   */
  public function getInstance(array $options) {
    // @todo BrokenHandler if $options['type'] not existing.

    $configuration = array(
      'field' => $options['field'],
    );

    return $this->createInstance($options['type'], $configuration);
  }
}
