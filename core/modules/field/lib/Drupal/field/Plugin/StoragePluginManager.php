<?php

/**
 * @file
 *
 * Definition of Drupal\field\Plugin\StoragePluginManager.
 */

namespace Drupal\field\Plugin;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Plugin\Discovery\CacheDecorator;
use Drupal\Core\Plugin\Discovery\HookDiscovery;
use Drupal\Component\Plugin\Factory\ReflectionFactory;

/**
 * 'Field storage' plugin type manager.
 */
class StoragePluginManager extends PluginManagerBase {

  protected $defaults = array(
    'settings' => array(),
  );

  protected $cache_bin = 'field';
  protected $cache_key = 'field_storage_types';
  protected $hook = 'field_storage_info2';

  public function __construct() {
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    $this->factory = new ReflectionFactory($this);
  }

  /**
   * Clear cached definitions.
   * @todo A cleaner way should be baked within CacheDecorator.
   */
  public function clearDefinitions() {
    // Clear 'static' data by creating a new object.
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    cache($this->cache_bin)->delete($this->cache_key);
  }


  /**
   * Overrides PluginManagerBase::getInstance().
   * @todo This takes the place of a Mapper - Do we want a real Mapper instead ?
   */
  public function getInstance(array $options) {
    // @todo BrokenHandler if $options['type'] not existing.
    
    $configuration = array(
      'settings' => $options['settings'],
    );

    return $this->createInstance($options['type'], $configuration);
  }
}
