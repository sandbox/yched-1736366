<?php

/**
 * @file
 *
 * Definition of Drupal\field\Plugin\WidgetPluginManager.
 */

namespace Drupal\field\Plugin;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Plugin\Discovery\CacheDecorator;
use Drupal\Core\Plugin\Discovery\HookDiscovery;
use Drupal\Component\Plugin\Factory\ReflectionFactory;

/**
 * 'Field widget' plugin type manager.
 */
class WidgetPluginManager extends PluginManagerBase {

  protected $defaults = array(
    'settings' => array(),
    'multiple_values' => FALSE,
  );

  protected $cache_bin = 'field';
  protected $cache_key = 'field_widget_types';
  protected $hook = 'field_widget_info2';

  public function __construct() {
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    $this->factory = new ReflectionFactory($this);
  }

  /**
   * Clear cached definitions.
   * @todo A cleaner way should be baked within CacheDecorator.
   */
  public function clearDefinitions() {
    // Clear 'static' data by creating a new object.
    $this->discovery = new CacheDecorator(new HookDiscovery($this->hook), $this->cache_key, $this->cache_bin);
    cache($this->cache_bin)->delete($this->cache_key);
  }

 /**
   * Overrides PluginManagerBase::getInstance().
   * @todo This takes the place of a Mapper - Do we want a real Mapper instead ?
   */
  public function getInstance(array $options) {
    $instance = $options['instance'];
    $type = $options['type'];

    $definition = $this->getDefinition($type);
    $field_handler = $instance->field()->handler();

    // Switch back to default widget if either:
    // - $type_info doesn't exist (the widget type is unknown),
    // - the field type is not allowed for the widget.
    if (!isset($definition['class']) || !in_array($field_handler->getPluginId(), $definition['field types'])) {
      // Grab the default widget for the field type.
      $field_type_definition = $field_handler->getDefinition();
      $type = $field_type_definition['default_widget'];
    }

    $configuration = array(
      'instance' => $instance,
      'settings' => $options['settings'],
    );

    return $this->createInstance($type, $configuration);
  }
}
