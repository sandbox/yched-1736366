<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\FieldType\FieldHandlerInterface.
 */

namespace Drupal\field\Field\FieldType;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\entity\EntityInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface definition for "Field type" plugins.
 */
interface FieldHandlerInterface extends PluginInspectionInterface {

  /**
   * @todo.
   */
  public function schema();

  /**
   * @todo
   */
  public function itemIsEmpty(array $item);

  /**
   * @todo
   * @todo hook_field_settings_form() received the $instance as well. Shouldn't be needed ?
   */
  public function settingsForm(array $form, array &$form_state);


  /**
   * @todo
   */
  public function instanceSettingsForm(FieldInstanceInterface $instance, array $form, array &$form_state);

  /**
   * Massage loaded field values.
   *
   * This method operates on multiple entities. The $entities, $instances and
   * $items parameters are arrays keyed by entity ID. For performance reasons,
   * information for all entities should be loaded in a single query where
   * possible.
   *
   * Note that the changes made to the field values get cached by the field
   * cache for subsequent loads. You should never load fieldable entities within
   * this method, since this is likely to cause infinite recursions. Use the
   * prepareView() method instead.
   *
   * There is no return value. Changes or additions to the field values are made
   * by altering the $items parameter by reference.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param $entities
   *   Array of entities being loaded, keyed by entity ID.
   * @param $instances
   *   Array of FieldInstance objects instance for each entity, keyed by entity
   *   ID.
   * @param $langcode
   *   The language code associated with $items.
   * @param $items
   *   Array of field values already loaded for the entities, keyed by entity
   *   ID. Store your changes in this parameter (passed by reference).
   * @param $age
   *   FIELD_LOAD_CURRENT to load the most recent revision for all fields, or
   *   FIELD_LOAD_REVISION to load the version indicated by each entity.
   */
  public function load($entity_type, array $entities, array $instances, $langcode, array &$items, $age);

    /**
   * Prepare field values prior to display.
   *
   * This method is invoked before the field values are handed to formatters
   * for display, and responsible for calling the formatters' own prepareView()
   * method.
   *
   * This method operates on multiple entities. The $entities, $instances and
   * $items parameters are arrays keyed by entity ID. For performance reasons,
   * information for all entities should be loaded in a single query where
   * possible.
   *
   * Make changes or additions to field values by altering the $items parameter
   * by reference. There is no return value.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param $entities
   *   Array of entities being displayed, keyed by entity ID.
   * @param $instances
   *   Array of instance objects, keyed by entity ID.
   * @param $langcode
   *   The language associated to $items.
   * @param $items
   *   $entity->{$field['field_name']}, or an empty array if unset.
   * @param $display
   *   Can be either the name of a view_mode, or an array of display settings as
   *   accepted by the FieldInterface::setDisplayConfig() method.
   */
  public function prepareView($entity_type, array $entities, array $instances, $langcode, array &$items, $display);

  /**
   * Validate field values.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated to $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   * @param $errors
   *   The array of errors, keyed by field name and by value delta, that have
   *   already been reported for the entity. The function should add its errors
   *   to this array. Each error is an associative array, with the following
   *   keys and values:
   *   - 'error': an error code (should be a string, prefixed with the module name)
   *   - 'message': the human readable message to be displayed.
   */
  public function validate($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, array &$errors);

  /**
   * Define custom presave behavior for this module's field types.
   *
   * Make changes or additions to field values by altering the $items parameter by
   * reference. There is no return value.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   */
  public function presave($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items);

  /**
   * Defines custom insert behavior for the field type.
   *
   * This method is invoked from field_attach_insert(), during the process of
   * inserting an entity object (node, taxonomy term, etc.). It is invoked just
   * before the data for this field on the particular entity object is inserted
   * into field storage. Only field types that are storing or tracking
   * information outside the standard field storage mechanism need to implement
   * this method.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   */
  public function insert($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items);

  /**
   * Defines custom update behavior for the field type.
   *
   * This method is invoked from field_attach_delete(), during the process of
   * deleting an entity object (node, taxonomy term, etc.). It is invoked just
   * before the data for this field on the particular entity object is deleted
   * from field storage. Only field modules that are storing or tracking
   * information outside the standard field storage mechanism need to implement
   * this method.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   */
  public function update($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items);

  /**
   * Defines custom delete behavior for the field type.
   *
   * This method is called just before the data is deleted from field storage.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   */
  public function delete($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items);

  /**
   * Defines custom revision delete behavior for the field type.
   *
   * This mathod is called just before the data is deleted from field storage,
   * for fieldable types that are versioned.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   */
  public function deleteRevision($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items);

  /**
   * Copies source field values into the entity to be prepared.
   *
   * @param $entity_type
   *   The type of $entity; e.g. 'node' or 'user'.
   * @param $entity
   *   The entity to be prepared for translation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language the entity has to be translated in.
   * @param $items
   *   $entity->{$field['field_name']}[$langcode], or an empty array if unset.
   * @param $source_entity
   *   The source entity holding the field values to be translated.
   * @param $source_langcode
   *   The source language from which translate.
   */
  public function prepareTranslation($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items, $source_entity, $source_langcode);

}
