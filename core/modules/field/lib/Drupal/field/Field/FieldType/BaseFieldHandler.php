<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\FieldType\BaseFieldHandler.
 */

namespace Drupal\field\Field\FieldType;
use Drupal\field\Field\FieldInterface;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\entity\EntityInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Plugin\Discovery\DiscoveryInterface;

/**
 * Base class for 'field type' plugin implementations.
 */
abstract class BaseFieldHandler extends PluginBase implements FieldHandlerInterface {

  /**
   * @var FieldInterface
   */
  protected $field;

  public function __construct($plugin_id, DiscoveryInterface $discovery, FieldInterface $field) {
    parent::__construct(array(), $plugin_id, $discovery);

    $this->field = $field;
  }

  /**
   * Implements FieldHandlerInterface::schema().
   */
  public function schema() {
    return array();
  }

  /**
   * Implements FieldHandlerInterface::itemIsEmpty().
   */
  public function itemIsEmpty(array $item) {
    return TRUE;
  }

  /**
   * Implements FieldHandlerInterface::settingsForm().
   * @todo hook_field_settings_form() received the $instance as well. Shouldn't be needed ?
   */
  public function settingsForm(array $form, array &$form_state) {
    return array();
  }


  /**
   * Implements FieldHandlerInterface::instanceSettingsForm().
   */
  public function instanceSettingsForm(FieldInstanceInterface $instance, array $form, array &$form_state) {
    return array();
  }

  /**
   * Implements FieldHandlerInterface::load().
   *
   * @todo rename postLoad()?
   */
  public function load($entity_type, array $entities, array $instances, $langcode, array &$items, $age) { }

  /**
   * Implements FieldHandlerInterface::prepareView().
   */
  public function prepareView($entity_type, array $entities, array $instances, $langcode, array &$items, $display) {
    // Call formatter prepareView - we need to group entities by bundle, to be
    // sure the formatter and settings are the same.
    $bundles = array();
    $grouped_entities = array();
    $grouped_instances = array();
    $grouped_items = array();
    foreach ($instances as $id => $instance) {
      $bundle = $instance->bundle;
      $bundles[] = $bundle;
      $grouped_entities[$bundle][$id] = $entities[$id];
      // Within a given bundle, the instance is the same for all entities.
      $grouped_instances[$bundle] = $instance;
      // FormatterInterface::prepareView alters $items by reference.
      $grouped_items[$bundle][$id] = &$items[$id];
    }

    foreach ($bundles as $bundle) {
      $instance = $grouped_instances[$bundle];
      if ($formatter = $instance->formatter($display)) {
        $formatter->prepareView($entity_type, $grouped_entities[$bundle], $langcode, $grouped_items[$bundle]);
      }
    }
  }

  /**
   * Implements FieldHandlerInterface::validate().
   */
  public function validate($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, array &$errors) {
    $field = $this->field;

    // Filter out empty values.
    $field->filterItems($items);

    // Check that the number of values doesn't exceed the field cardinality. For
    // form submitted values, this can only happen with 'multiple value'
    // widgets.
    $cardinality = $field->cardinality;
    if ($cardinality != FIELD_CARDINALITY_UNLIMITED && count($items) > $cardinality) {
      $errors[$field->name][$langcode][0][] = array(
        'error' => 'field_cardinality',
        'message' => t('%name: this field cannot hold more than @count values.', array('%name' => $instance->label, '@count' => $cardinality)),
      );
    }
  }

  /**
   * Implements FieldHandlerInterface::insert().
   */
  public function insert($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items) {
    $field = $this->field;
    $field_name = $field->name;

    // Insert default value if no $entity->$field_name entry was provided. This
    // can happen with programmatic saves, or on form-based creation where the
    // current user doesn't have 'edit' permission for the field.

    // field_invoke_method() populates $items with an empty array if the $entity
    // has no entry for the field, so we check on the $entity itself. We also
    // check that the current field translation is actually defined before
    // assigning it a default value. This way we ensure that only the intended
    // languages get a default value. Otherwise we could have default values for
    // not yet open languages.
    if (empty($entity) || !property_exists($entity, $field_name) ||
      (isset($entity->{$field_name}[$langcode]) && count($entity->{$field_name}[$langcode]) == 0)) {
      $items = $instance->computeDefaultValue($entity_type, $entity, $langcode);
    }
  }

  /**
   * Implements FieldHandlerInterface::presave().
   */
  public function presave($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items) {
    // Filter out empty values.
    $this->field->filterItems($items);
  }

  /**
   * Implements FieldHandlerInterface::update().
   */
  public function update($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items) { }

  /**
   * Implements FieldHandlerInterface::delete().
   */
  public function delete($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items) { }

  /**
   * Implements FieldHandlerInterface::deleteRevision().
   */
  public function deleteRevision($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items) { }

  /**
   * Implements FieldHandlerInterface::prepareTranslation().
   */
  public function prepareTranslation($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items, $source_entity, $source_langcode) {
    $field_name = $this->field->name;

    // If the field is untranslatable keep using LANGUAGE_NOT_SPECIFIED.
    if ($langcode == LANGUAGE_NOT_SPECIFIED) {
      $source_langcode = LANGUAGE_NOT_SPECIFIED;
    }
    if (isset($source_entity->{$field_name}[$source_langcode])) {
      $items = $source_entity->{$field_name}[$source_langcode];
    }
  }
}
