<?php

/**
 * Definition of Drupal\field\Field\FieldInterface.
 */

namespace Drupal\field\Field;
use Drupal\field\Field\FieldType\FieldHandlerInterface;
use Drupal\field\Field\Storage\FieldStorageInterface;
use Drupal\entity\EntityInterface;

/**
 * Interface definition for field objects.
 */
interface FieldInterface {

  /**
   * Creates an object from a stored definition.
   *
   * @param $definition
   *   The instance definition.
   *
   * @return FieldInterface
   *   The field object.
   */
  public static function CreateFromConfig(array $definition);

  /**
   * Saves a field definition to the configuration layer.
   *
   * @return FieldInterface
   *   The saved field object.
   */
  public function save();

  /**
   * Marks a field instance and its data for deletion.
   *
   * @return FieldInterface
   *   The instance object.
   */
  public function delete();

  /**
   * Returns the field type handler.
   *
   * @return FieldHandlerInterface
   *   The field handler.
   */
  public function handler();

  /**
   * Returns the storage handler.
   *
   * @return FieldStorageInterface
   *   The storage handler.
   */
  public function storage();

  /**
   * @todo
   */
  public function getStorageConfig();

  /**
   * @todo
   *
   * @return FieldInterface
   */
  public function setStorageConfig(array $properties);

  /**
   * Returns the Field ID.
   *
   * @return
   *   The field ID.
   */
  public function id();

  /**
   * Indicates whether the field accpets multiple values.
   *
   * @return
   *   TRUE if the field accepts multiple values, FALSE if not.
   */
  public function isMultiple();

  /**
   * Indicates whether the field can appear in the entity type..
   *
   * @param $entity_type
   *   The entity type.
   *
   * @return
   *   TRUE if the field can appear in the entity type, FALSE if not..
   */
  public function isValidEntityType($entity_type);

  /**
   * @todo
   */
  public function getSettings();

  /**
   * @todo
   */
  public function getSetting($key);

  /**
   * @todo
   *
   * @return FieldInterface
   */
  public function setSettings(array $settings);

  /**
   * @todo
   *
   * @return FieldInterface
   */
  public function setSetting($key, $value);

  /**
   * @todo
   */
  public function filterItems(array &$items);

  /**
   * Determine whether a field has any data.
   *
   * @return
   *   TRUE if the field has data for any entity; FALSE otherwise.
   */
  public function hasData();

  /**
   * @todo.
   */
  public function schema();

  /**
   * @todo.
   */
  public function columns();

  /**
   * @todo.
   */
  public function storageDetails();

  /**
   * Returns the list of bundles where the field appears.
   *
   * @return
   *   An array with entity types as keys and the array of bundle names as
   *   values.
   */
  public function bundles();

  /**
   * Determines whether the user has access to a given field.
   *
   * @param string $op
   *   The operation to be performed. Possible values:
   *   - 'edit'
   *   - 'view'
   * @param string $entity_type
   *   The type of $entity; e.g., 'node' or 'user'.
   * @param $entity
   *   (optional) The entity for the operation.
   * @param $account
   *   (optional) The account to check, if not given use currently logged in user.
   *
   * @return
   *   TRUE if the operation is allowed;
   *   FALSE if the operation is denied.
   */
  public function access($op, $entity_type, EntityInterface $entity = NULL, $account = NULL);




  /**
   * Getters - deprectaed for now, we'll see how core standardizes.
   */

//  /**
//   * Returns the Field name.
//   *
//   * @return
//   *   The field ID.
//   */
//  public function name();
//
//  /**
//   * Returns the Field type.
//   *
//   * @return
//   *   The field type.
//   */
//  public function type();
//
//
//  /**
//   * Returns the Field cardinality.
//   *
//   * @return
//   *   The field cardinality.
//   */
//  public function cardinality();
//
//  /**
//   * Indicates whether the field is deleted.
//   *
//   * @return
//   *   TRUE if the field is deleted, FALSE if not.
//   */
//  public function isDeleted();
//
//  /**
//   * Indicates whether the field is translatable.
//   *
//   * @return
//   *   TRUE if the field is translatable, FALSE if not.
//   */
//  public function isTranslatable();
//
//  /**
//   * Indicates whether the field is locked.
//   *
//   * @return
//   *   TRUE if the field is locked, FALSE if not.
//   */
//  public function isLocked();
//
//  /**
//   * Returns the list of entity types the field can appear in.
//   *
//   * @return
//   *   The array of entity types the field can appear in, or an empty array if
//   *   the field has no restrictions.
//   */
//  public function entityTypes();
}
