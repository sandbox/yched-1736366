<?php

/**
 * Definition of Drupal\field\Field\FieldInstance.
 */

namespace Drupal\field\Field;
use Drupal\field\Field\FieldType\FieldHandlerInterface;
use Drupal\field\Field\Formatter\FormatterInterface;
use Drupal\field\Field\FieldWidgetBridge;
use Drupal\field\Field\Widget\WidgetInterface;
use Drupal\entity\EntityInterface;

/**
 * Class for field instance objects.
 */
class FieldInstance implements FieldInstanceInterface {

  /**
   * @var FieldHandlerInterface
   */
  protected $handler;

  protected $id;

  /**
   * @var FieldInterface
   */
  protected $field;

  protected $fieldId;

  public $entityType;

  public $bundle;

  public $label;

  public $description;

  public $required;

  public $deleted;

  public $defaultValue;

  public $defaultValueFunction;

  protected $widgetConfig;

  protected $displayConfig;

  protected $settings;

  protected $defaultSettingsMerged;

  /**
   * @var WidgetInterface
   */
  protected $widget;

  /**
   * @var Drupal\field\Field\FieldWidgetBridge
   */
  protected $widgetBridge;

  // @todo people want to use field name rather than id.
  public function __construct($entity_type, $bundle, $field_id) {
    $this->fieldId = $field_id;
    $this->entityType = $entity_type;
    $this->bundle = $bundle;

    $this->id = NULL;
    $this->label = '';
    $this->description = '';
    $this->required = FALSE;
    $this->deleted = FALSE;
    $this->defaultValue = array();
    $this->defaultValueFunction = '';
    $this->settings = array();
    $this->widgetConfig = array();
    $this->displayConfig = array();

    $this->definition = array();
  }

  /**
   * Implements FieldInstanceInterface::CreateFromConfig().
   */
  public static function CreateFromConfig(array $definition) {
    $instance = new static($definition['entity_type'], $definition['bundle'], $definition['field_id']);

    // @todo do we really need instance ids ?
    $instance->id = $definition['id'];
    $instance->label = $definition['label'];
    $instance->description = $definition['description'];
    $instance->required = !empty($definition['required']);
    $instance->deleted = !empty($definition['deleted']);
    $instance->defaultValue = !empty($definition['default_value']) ? $definition['default_value'] : array();
    $instance->defaultValueFunction = !empty($definition['default_value_function']) ? $definition['default_value_function'] : '';
    $instance->settings = $definition['settings'];
    $instance->widgetConfig = $definition['widget'];
    $instance->displayConfig = $definition['display'];

    // @todo: temporary
    $instance->definition = $definition;

    return $instance;
  }

  /**
   * Implements FieldInstanceInterface::save().
   */
  public function save() {
    if ($this->id) {
      $update = TRUE;

      // Check that the specified field exists.
      // @todo Include deleted ?
      // @todo use $this->field() ?
      $field_definitions = field_read_fields(array('id' => $this->fieldId), array('include_deleted' => 1));
      if (empty($field_definitions)) {
        throw new \FieldException('Attempt to update an instance of a field that does not exist or is currently inactive.');
      }
      $field = Field::CreateFromConfig(current($field_definitions));

      // Check that the field instance exists.
      // @todo Include deleted ? inactive ?
      $definitions = field_read_instances(array('id' => $this->id), array('include_deleted' => TRUE));
      if (empty($definitions)) {
        throw new \FieldException('Attempt to update a nonexistent instance.');
      }
      $prior_instance = self::CreateFromConfig(current($definitions));

      if ($this->entityType != $prior_instance->entityType) {
        throw new \FieldException('Cannot change an existing instance\'s entity type.');
      }
      if ($this->bundle != $prior_instance->bundle) {
        throw new \FieldException('Cannot change an existing instance\'s bundle.');
      }
    }
    else {
      $update = FALSE;

      // Check that the specified field exists.
      // @todo Include deleted ?
      // @todo use $this->field() ?
      $field_definitions = field_read_fields(array('id' => $this->fieldId));
      if (empty($field_definitions)) {
        throw new \FieldException('Attempt to create an instance of a field that does not exist or is currently inactive or deleted.');
      }
      $field = Field::CreateFromConfig(current($field_definitions));

      // Check that the field can be attached to this entity type.
      if (!$field->isValidEntityType($this->entityType)) {
        throw new FieldException(format_string('Attempt to create an instance of field @field_name on forbidden entity type @entity_type.', array('@field_name' => $field->name, '@entity_type' => $this->entityType)));
      }

      // Do *not* prevent creating a field on non-existing bundles, that would
      // prevent field updates while some entity types or bundles are
      // temporarily disabled.

      // Ensure the field instance is unique within the bundle.
      // We only check for active instances, since adding an instance of an
      // inactive field is not supported.
      $existing_definition = field_read_instance($this->entityType, $field->name, $this->bundle);
      if (!empty($existing_definition)) {
        throw new FieldException(format_string('Attempt to create an instance of field @field_name on bundle @entity_type/@bundle that already has an instance of that field.', array('@field_name' => $field->name, '@entity_type' => $this->entityType, '@bundle' => $this->bundle)));
      }
    }

    // Assemble the 'widget' part.
    $widget_record = $this->getWidgetConfig();
    // Make sure all settings are populated.
    $widget_record['settings'] += field_info_widget_settings($widget_record['type']);
    // If no weight specified, make sure the field sinks at the bottom.
    if (!isset($widget_record['weight'])) {
      $max_weight = field_info_max_weight($this->entityType, $this->bundle, 'form');
      $widget_record['weight'] = isset($max_weight) ? $max_weight + 1 : 0;
    }

    // Assemble the 'display' part.
    $displays_record = array();
    // Make sure there are at least display settings for the 'default' view
    // mode.
    if (!$this->getDisplayConfig('default')) {
      $this->setDisplayConfig('default', array('label' => 'above'));
    }
    // Include all view modes present in the definition.
    foreach (array_keys($this->displayConfig) as $view_mode) {
      $display_record = $this->getDisplayConfig($view_mode);
      // Make sure all settings are populated.
      $display_record['settings'] += field_info_formatter_settings($display_record['type']);
      // If no weight specified, make sure the field sinks at the bottom.
      if (!isset($display_record['weight'])) {
        $max_weight = field_info_max_weight($this->entityType, $this->bundle, $view_mode);
        $display_record['weight'] = isset($max_weight) ? $max_weight + 1 : 0;
      }
      $displays_record[$view_mode] = $display_record;
    }

    // Assemble the record.
    $record = array(
      'field_id' => $this->fieldId,
      'field_name' => $field->name,
      'entity_type' => $this->entityType,
      'bundle' => $this->bundle,
      'deleted' => 0,
      // @todo we no longer save 'the rest' in 'data'. Might be an issue for D7 upgrades.
      'data' => array(
        // @todo previously defaulted to field_name ?
        'label' => $this->label,
        'description' => $this->description,
        'required' => $this->required,
        'default_value' => $this->defaultValue,
        'default_value_function' => $this->defaultValueFunction,
        // Make sure all settings are populated.
        'settings' => $this->getSettings(),
        'widget' => $widget_record,
        'display' => $displays_record,
      ),
    );

    if ($update) {
      // Save the record.
      $record['id'] = $this->id;
      drupal_write_record('field_config_instance', $record, array('id'));

      // Invoke external hooks after the cache is cleared for API consistency.
      // @todo rename the hooks.
      field_cache_clear();
      module_invoke_all('field_update_instance', $this, $prior_instance);
    }
    else {
      // Save the record and get an id back.
      drupal_write_record('field_config_instance', $record);
      $this->id = $record['id'];

      // Invoke external hooks after the cache is cleared for API consistency.
      // @todo rename the hooks.
      field_cache_clear();
      module_invoke_all('field_create_instance', $this);
    }

    return $this;
  }

  /**
   * Implements FieldInstanceInterface::delete().
   */
  public function delete($field_cleanup = TRUE) {
    if ($this->deleted) {
      // @todo Exception ?
      return $this;
    }

    $field = $this->field();
    $bundles = $field->bundles();

    $this->deleted = TRUE;

    // Mark the field instance for deletion.
    db_update('field_config_instance')
      ->fields(array('deleted' => 1))
      ->condition('field_name', $field->name)
      ->condition('entity_type', $this->entityType)
      ->condition('bundle', $this->bundle)
      ->execute();

    // Clear the cache.
    field_cache_clear();

    // Let the storage backend mark instance data for deletion.
    module_invoke($field->definition['storage']['module'], 'field_storage_delete_instance', $this);

    // Let modules react to the deletion of the instance.
    module_invoke_all('field_delete_instance', $this);

    // Delete the field itself if we just deleted its last instance.
    if ($field_cleanup && empty($bundles)) {
      $field->delete();
    }

    return $this;
  }

  /**
   * Implements FieldInstanceInterface::handler().
   */
  public function handler() {
    // @todo Optimization : a ref to the $handler should probably be kept within the instance.
    return $this->field()->handler();
  }

  /**
   * Implements FieldInstanceInterface::widget().
   */
  public function widget() {
    if (empty($this->widget)) {
      $definition = $this->getWidgetConfig();
      $options = array(
        'instance' => $this,
        'type' => $definition['type'],
        'settings' => $definition['settings'],
        'weight' => $definition['weight'],
      );
      $this->widget = field_get_plugin_manager('widget')->getInstance($options);
    }

    return $this->widget;
  }

  /**
   * Implements FieldInstanceInterface::widgetBridge().
   */
  public function widgetBridge() {
    if (empty($this->widgetBridge)) {
      $definition = $this->getWidgetConfig();
      $this->widgetBridge = new FieldWidgetBridge($this, $this->widget(), $definition['weight']);
    }
    return $this->widgetBridge;
  }


  /**
   * Implements FieldInstanceInterface::getWidgetConfig().
   */
  public function getWidgetConfig() {
    // Ensure defaults. This is not done in the constructor to avoid needlessly
    // instantiating the field object.
    $this->widgetConfig += array(
      'settings' => array(),
      'weight' => NULL,
    );
    if (empty($this->widgetConfig['type'])) {
      $field_type_info = $this->handler()->getDefinition();
      $this->widgetConfig['type'] = $field_type_info['default_widget'];
    }

    return $this->widgetConfig;
  }

  /**
   * Implements FieldInstanceInterface::setWidgetConfig().
   */
  public function setWidgetConfig(array $properties) {
    $this->widgetConfig = $properties;
    // The widget object will need to be recreated.
    unset($this->widget);

    return $this;
  }

  /**
   * Implements FieldInstanceInterface::formatter().
   */
  public function formatter($display) {
    if (is_string($display)) {
      // A view mode was provided  Switch to 'default' if the view mode is not
      // configured to use dedicated settings.
      $view_mode = $display;
      $view_mode_settings = field_view_mode_settings($this->entityType, $this->bundle);
      $actual_mode = (!empty($view_mode_settings[$view_mode]['custom_settings']) ? $view_mode : 'default');

      if (isset($this->formatters[$actual_mode])) {
        return $this->formatters[$actual_mode];
      }

      $definition = $this->getDisplayConfig($actual_mode);

      // Let modules alter the display settings.
//      $context = array(
//        'entity_type' => $this->entityType,
//        'field' => $this->field,
//        'instance' => $this,
//        // @todo ouch.
//        'entity' => $entity,
//        'view_mode' => $view_mode,
//      );
//      drupal_alter(array('field_display', 'field_display_' . $this->entityType), $definition, $context);
    }
    else {
      // Make sure defaults are present.
      $definition = $display + array(
        'settings' => array(),
        'weight' => 0,
        'label' => 'above',
      );
      $view_mode = '_custom_display';
    }

    if (!empty($definition['type']) && $definition['type'] != 'hidden') {
      $options = array(
        'instance' => $this,
        'type' => $definition['type'],
        'settings' => $definition['settings'],
        'weight' => $definition['weight'],
        'label' => $definition['label'],
        'view_mode' => $view_mode,
      );
      $formatter = field_get_plugin_manager('formatter')->getInstance($options);
    }
    else {
      $formatter = NULL;
    }

    if (isset($actual_mode)) {
      $this->formatters[$actual_mode] = $formatter;
    }

    return $formatter;
  }

  /**
   * Implements FieldInstanceInterface::getDisplayConfig().
   */
  public function getDisplayConfig($view_mode) {
    // If not specified, the field is considered hidden.
    if (!isset($this->displayConfig[$view_mode])) {
      return;
    }

    // Ensure defaults. This is not done in the constructor to avoid needlessly
    // instantiating the field object.
    $this->displayConfig[$view_mode] += array(
      'settings' => array(),
      'weight' => NULL,
      'label' => 'above',
    );
    if (empty($this->displayConfig[$view_mode]['type'])) {
      $field_type_info = $this->handler()->getDefinition();
      $this->displayConfig[$view_mode]['type'] = $field_type_info['default_formatter'];
    }

    return $this->displayConfig[$view_mode];
  }

  /**
   * Implements FieldInstanceInterface::setDisplayConfig().
   */
  public function setDisplayConfig($view_mode, array $properties) {
    if (empty($properties) || (isset($properties['type']) && $properties['type'] == 'hidden')) {
      unset($this->displayConfig[$view_mode]);
    }
    else {
      $this->displayConfig[$view_mode] = $properties;
    }

    // The formatter object will need to be recreated.
    unset($this->formatters[$view_mode]);

    return $this;
  }

  /**
   * Implements FieldInstanceInterface::id().
   */
  public function id() {
    return $this->id;
  }

  /**
   * Implements FieldInstanceInterface::fieldId().
   */
  public function fieldId() {
    return $this->fieldId;
  }

  /**
   * Implements FieldInstanceInterface::field().
   */
  public function field() {
    if (empty($this->field)) {
      // @todo what if fieldId doesn't exist ? is inactive ?
      $this->field = _field_info_field_cache()->getFieldById($this->fieldId);
    }
    return $this->field;
  }


  /**
   * Implements FieldInstanceInterface::computeDefaultValue().
   */
  public function computeDefaultValue($entity_type, EntityInterface $entity, $langcode = NULL) {
    $items = array();

    // Use the callback if present, else use the literal value.
    if (!empty($this->defaultValueFunction)) {
      $items = call_user_func($this->defaultValueFunction, $entity_type, $entity, $this, $langcode);
    }
    elseif (!empty($this->defaultValue)) {
      $items = $this->defaultValue;
    }

    return $items;
  }

  /**
   * @todo.
   */
  public function getSettings() {
    if (empty($this->defaultSettingsMerged)) {
      $this->settings += $this->getDefaultSettings();
      $this->defaultSettingsMerged = TRUE;
    }
    return $this->settings;
  }

  /**
   * @todo.
   */
  public function getSetting($key) {
    $settings = $this->getSettings();
    return isset($settings[$key]) ? $settings[$key] : NULL;
  }

  /**
   * @todo.
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    $this->defaultSettingsMerged = FALSE;

    return $this;
  }

  /**
   * @todo.
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;

    return $this;
  }

  /**
   * @todo.
   */
  public function getDefaultSettings() {
    $definition = $this->handler()->getDefinition();
    return $definition['instance_settings'];
  }


  /**
   * Getters - deprectaed for now, we'll see how core standardizes.
   */

//  /**
//   * Implements FieldInstanceInterface::entityType().
//   */
//  public function entityType() {
//    return $this->entityType;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::bundle().
//   */
//  public function bundle() {
//    return $this->bundle;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::label().
//   */
//  public function label() {
//    return $this->label;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::description().
//   */
//  public function description() {
//    return $this->description;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::isRequired().
//   */
//  public function isRequired() {
//    return $this->required;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::isDeleted().
//   */
//  public function isDeleted() {
//    return $this->deleted;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::defaultValue().
//   */
//  public function defaultValue() {
//    return $this->defaultValue;
//  }
//
//  /**
//   * Implements FieldInstanceInterface::defaultValueFunction().
//   */
//  public function defaultValueFunction() {
//    return $this->defaultValueFunction;
//  }
}
