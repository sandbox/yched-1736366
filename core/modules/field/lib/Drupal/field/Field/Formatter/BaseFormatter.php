<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\Formatter\BaseFormatter.
 */

namespace Drupal\field\Field\Formatter;
use Drupal\field\Field\FieldInterface;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\entity\EntityInterface;
use Drupal\field\Field\PluginSettingsBase;
use Drupal\Component\Plugin\Discovery\DiscoveryInterface;

/**
 * Base class for 'Field formatter' plugin implementations.
 */
abstract class BaseFormatter extends PluginSettingsBase implements FormatterInterface {

  /**
   * @var FieldInterface
   */
  protected $field;

  /**
   * @var FieldInstanceInterface
   */
  protected $instance;

  public function __construct($plugin_id, DiscoveryInterface $discovery, FieldInstanceInterface $instance, array $settings, $weight, $label, $view_mode) {
    parent::__construct(array(), $plugin_id, $discovery);

    $this->instance = $instance;
    $this->field = $this->instance->field();
    $this->settings = $settings;
    $this->weight = $weight;
    $this->label = $label;
    $this->view_mode = $view_mode;
  }

  /**
   * Implements FormatterInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) { }

  /**
   * Implements FormatterInterface::settingsSummary().
   */
  public function settingsSummary() { }

  /**
   * Implements FormatterInterface::prepareView().
   */
  public function prepareView($entity_type, array $entities, $langcode, array &$items) { }

  /**
   * Implements FormatterInterface::view().
   */
  public function view($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, $view_mode) {
    $addition = array();

    $elements = $this->viewElements($entity_type, $entity, $langcode, $items);

    if ($elements) {
      $info = array(
        '#theme' => 'field',
        '#weight' => $this->weight,
        '#title' => $this->instance->label,
        '#access' => $this->field->access('view', $entity_type, $entity),
        '#label_display' => $this->label,
        '#view_mode' => $view_mode,
        '#language' => $langcode,
        '#field_name' => $this->field->name,
        '#field_type' => $this->field->type,
        '#field_translatable' => $this->field->translatable,
        '#entity_type' => $entity_type,
        '#bundle' => $entity->bundle(),
        '#object' => $entity,
        '#items' => $items,
        '#formatter' => $this->getPluginId(),
      );

      $addition[$this->field->name] = array_merge($info, $elements);
    }

    return $addition;
  }
}
