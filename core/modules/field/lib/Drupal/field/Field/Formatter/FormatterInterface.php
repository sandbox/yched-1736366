<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\Formatter\FormatterInterface.
 */

namespace Drupal\field\Field\Formatter;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\entity\EntityInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface definition for "Field formatter" plugins.
 */
interface FormatterInterface extends PluginInspectionInterface {

  /**
   * @todo
   */
  public function getSettings();

  /**
   * @todo
   */
  public function getSetting($key);

  /**
   * @todo
   */
  public function setSettings(array $settings);

  /**
   * @todo
   */
  public function setSetting($key, $value);

  /**
   * @todo
   */
  public function settingsForm(array $form, array &$form_state);

  /**
   * @todo
   */
  public function settingsSummary();

  public function prepareView($entity_type, array $entities, $langcode, array &$items);

  /**
   * Builds a renderable array for one field on one entity instance.
   *
   * @param $entity_type
   *   The type of $entity; e.g. 'node' or 'user'.
   * @param Drupal\entity\EntityInterface $entity
   *   A single object of type $entity_type.
   * @param FieldInstanceInterface $instance
   *   The field instance.
   * @param $langcode
   *   The language associated to $items.
   * @param $items
   *   Array of field values already loaded for the entities, keyed by entity id.
   * @param $display
   *   Can be either:
   *   - the name of a view mode;
   *   - or an array of custom display settings, as accepted by
   *     FieldInstanceInterface::setDisplayConfig().
   */
  public function view($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, $display);

  /**
   * @todo
   */
  public function viewElements($entity_type, EntityInterface $entity, $langcode, array $items);

}
