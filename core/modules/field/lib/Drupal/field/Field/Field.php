<?php

/**
 * Definition of Drupal\field\Field\Field.
 */

namespace Drupal\field\Field;
use Drupal\field\Field\FieldType\FieldHandlerInterface;
use Drupal\field\Plugin\FieldDiscovery;
use Drupal\entity\EntityFieldQuery;
use Drupal\entity\EntityInterface;

/**
 * Class for field objects.
 */
class Field implements FieldInterface {

  /**
   * Target helpers for field_invoke_method().
   *
   * @todo revisit...
   */
  static function FieldHandlerTarget() {
    // Note : accepts $object as a $field or $instance.
    return function ($object) {
      return $object->handler();
    };
  }
  static function WidgetTarget() {
    return function (FieldInstanceInterface $instance) {
      return $instance->widgetBridge();
    };
  }
  static function FormatterTarget($display) {
    return function (FieldInstanceInterface $instance) use ($display) {
      return $instance->formatter($display);
    };
  }

  /**
   * @var FieldHandlerInterface
   */
  protected $handler;

  protected $id;

  public $name;

  public $type;

  public $cardinality;

  public $deleted;

  public $translatable;

  public $locked;

  public $entityTypes;

  protected $storageDetails;

  public function __construct($name, $type) {
    $this->name = $name;
    $this->type = $type;

    $this->id = NULL;
    $this->cardinality = 1;
    $this->deleted = FALSE;
    $this->translatable = FALSE;
    $this->locked = FALSE;
    $this->entityTypes = array();
    $this->settings = array();
    $this->storageConfig = array();

    $this->definition = array();
  }

  /**
   * Implements FieldInterface::CreateFromConfig().
   */
  public static function CreateFromConfig(array $definition) {
    $field = new static($definition['field_name'], $definition['type']);

    $field->id = $definition['id'];
    $field->cardinality = $definition['cardinality'];
    $field->deleted = !empty($definition['deleted']);
    $field->translatable = !empty($definition['translatable']);
    $field->locked = !empty($definition['locked']);
    $field->entityTypes = !empty($definition['entity_types']) ? $definition['entity_types'] : array();
    $field->settings = $definition['settings'];
    $field->storageConfig = $definition['storage'];

    // @todo: temporary
    $field->definition = $definition;

    return $field;
  }

  /**
   * Implements FieldInterface::save().
   */
  public function save() {
    $field_type_info = field_info_field_types($this->type);

    if ($this->id) {
      $update = TRUE;

      // Check that the specified field exists.
      // @todo Include deleted ? inactive ?
      $definitions = field_read_fields2(array('id' => $this->id), array('include_deleted' => 1));
      if (empty($definitions)) {
        throw new \FieldException('Attempt to update a non-existent field.');
      }
      $definition = current($definitions);

      $prior_field = self::CreateFromConfig($definition);

      // @todo can this really happen if there are no setters in name and type ?
      if ($this->name != $prior_field->name) {
        throw new \FieldException('Cannot change an existing field\'s name.');
      }
      if ($this->type != $prior_field->type) {
        throw new \FieldException('Cannot change an existing field\'s type.');
      }
      if ($this->entityTypes != $prior_field->entityTypes) {
        throw new \FieldException('Cannot change an existing field\'s entityTypes property.');
      }
//      if ($field['storage']['type'] != $prior_field['storage']['type']) {
//       throw new FieldException("Cannot change an existing field's storage type.");
//      }

      // Let any module forbid the update by throwing an exception.
      // @todo those should be direct method calls on the field type handler and
      // storage handler.
      $has_data = $this->hasData();
      module_invoke_all('field_update_forbid', $this, $prior_field, $has_data);
    }
    else {
      $update = FALSE;

      // Field name starts with a letter and only has valid characters.
      if (!preg_match('/^[_a-z]+[_a-z0-9]*$/', $this->name)) {
        // @todo Move FieldException to a proper class.
        throw new \FieldException(format_string('Attempt to create a field with an invalid name: @name.', array('@name' => $this->name)));
      }

      // Field name cannot be longer than 32 characters. We use drupal_strlen()
      // because the DB layer assumes that column widths are given in
      // characters, not bytes.
      if (drupal_strlen($this->name) > 32) {
        throw new \FieldException(format_string('Attempt to create a field with a name longer than 32 characters: @name.', array('@name' => $this->name)));
      }

      // Ensure the field name is unique over active and disabled fields.
      // We do not care about deleted fields.
      $prior_field = field_read_field2($this->name, array('include_inactive' => TRUE));
      if (!empty($prior_field)) {
        $message = $prior_field['active']?
          format_string('Attempt to create field name @name which already exists and is active.', array('@name' => $this->name)):
          format_string('Attempt to create field name @name which already exists, although it is inactive.', array('@name' => $this->name));
        throw new \FieldException($message);
      }

      // Disallow reserved field names. This can't prevent all field name
      // collisions with existing entity properties, but some is better than
      // none.
      // @todo Revisit ?
      foreach (entity_get_info() as $entity_type => $info) {
        if (in_array($this->name, $info['entity keys'])) {
          throw new \FieldException(format_string('Attempt to create field name @name which is reserved by entity type @entity_type.', array('@name' => $this->name, '@entity_type' => $entity_type)));
        }
      }

      // Check that the field type is known.
      if (!$field_type_info) {
        throw new \FieldException(t('Attempt to create a field of unknown type @type.', array('@type' => $this->type)));
      }

      // Check that the storage type is known.
      // @todo
//      $storage_type = field_info_storage_types($field['storage']['type']);
//      if (!$storage_type) {
//        throw new \FieldException(t('Attempt to create a field with unknown storage type @type.', array('@type' => $field['storage']['type'])));
//      }
    }

    // Assemble the 'storage' part.
    $storage = $this->storage();
    $storage_config = $this->getStorageConfig();
    // Make sure all settings are populated.
    $storage_config['settings'] += $storage->getSettings();
    $schema = $this->schema();
    $storage_definition = $storage->getDefinition();

    // Assemble the definition record.
    $record = array(
      'field_name' => $this->name,
      'type' => $this->type,
      'cardinality' => $this->cardinality,
      'translatable' => $this->translatable,
      'locked' => $this->locked,
      'deleted' => $this->deleted,
      'module' => isset($field_type_info['module']) ? $field_type_info['module'] : 'text',
      'active' => TRUE,
      'storage_type' => $storage_config['type'],
      'storage_module' => $storage_definition['module'],
      'storage_active' => 1,

      // @todo we no longer save 'the rest' in 'data'. Might be an issue for D7 upgrades.
      'data' => array(
        'entity_types' => $this->entityTypes,
        // Make sure all settings are populated.
        'settings' => $this->getSettings(),
        'storage' => array(
          'settings' => $storage_config['settings'],
        ),
        // 'indexes' can be both specified by the field type, and adjusted for
        // each field.
        // @todo move this to a dedicated indexes() method.
        'indexes' => (isset($this->definition['indexes']) ? (array) $this->definition['indexes'] : array()) + $schema['indexes'],
      ),
    );

    if ($update) {
      // Tell the storage engine to update the field. Do this before saving
      // the new definition since it still might fail.
      $this->storage()->updateField($this, $prior_field, $has_data);

      // Save the record.
      $record['id'] = $this->id;
      drupal_write_record('field_config', $record, array('id'));

      // Invoke external hooks after the cache is cleared for API consistency.
      // @todo rename the hooks.
      field_cache_clear(TRUE);
      module_invoke_all('field_update_field', $this, $prior_field, $has_data);
    }
    else {
      // Save the record first and get an id back.
      drupal_write_record('field_config', $record);
      $this->id = $record['id'];

      // Let the storage backend react now that we have an id.
      try {
        $this->storage()->createField($this);
      }
      catch (Exception $e) {
        // If storage creation failed, remove the field_config record before
        // rethrowing the exception.
        db_delete('field_config')
          ->condition('id', $this->id)
          ->execute();
        throw $e;
      }

      // Invoke external hooks after the cache is cleared for API consistency.
      // @todo rename the hooks.
      field_cache_clear(TRUE);
      module_invoke_all('field_create_field', $this);
    }

    return $this;
  }

  /**
   * Implements FieldInterface::delete().
   */
  public function delete() {
    if ($this->deleted) {
      // @todo Exception ?
      return $this;
    }

    // Delete the instances before we mark the field itself as deleted.
    foreach ($this->bundles() as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        $instance = field_info_instance2($entity_type, $this->name, $bundle);
        // Delete the instance, but no not loop to the deletion of the field
        // when the last one gets deleted.
        $instance->delete(FALSE);
      }
    }

    $this->deleted = TRUE;

    // Mark the field for deletion.
    db_update('field_config')
      ->fields(array('deleted' => 1))
      ->condition('field_name', $this->name)
      ->execute();

    field_cache_clear(TRUE);

    // Let the storage backend mark field data for deletion.
    module_invoke($this->definition['storage']['module'], 'field_storage_delete_field', $this);

    // Let modules react to the deletion of the field.
    module_invoke_all('field_delete_field', $this);

    return $this;
  }

  /**
   * Implements FieldInterface::handler().
   */
  public function handler() {
    if (empty($this->handler)) {
      $options = array(
        'type' => $this->type,
        'field' => $this,
      );
      $this->handler = field_get_plugin_manager('field_type')->getInstance($options);
    }

    return $this->handler;
  }

  /**
   * Implements FieldInterface::storage().
   */
  public function storage() {
    if (empty($this->storage)) {
      $definition = $this->getStorageConfig();
      $options = array(
        'type' => $definition['type'],
        'settings' => $definition['settings'],
      );
      $this->storage = field_get_plugin_manager('storage')->getInstance($options);
    }
    return $this->storage;
  }

  /**
   * Implements FieldInterface::getStorageConfig().
   */
  public function getStorageConfig() {
    // Ensure defaults.
    $this->storageConfig += array(
      'settings' => array(),
    );
    if (empty($this->storageConfig['type'])) {
      $this->storageConfig['type'] = variable_get('field_storage_default', 'field_sql_storage');
    }

    return $this->storageConfig;
  }

  /**
   * Implements FieldInterface::setStorageConfig().
   */
  public function setStorageConfig(array $properties) {
    $this->storageConfig = $properties;
    // The widget object will need to be recreated.
    unset($this->storage);

    return $this;
  }

  /**
   * Implements FieldInterface::id().
   */
  public function id() {
    return $this->id;
  }

  /**
   * Implements FieldInterface::isMultiple().
   */
  public function isMultiple() {
    // @todo FIELD_CARDINALITY_UNLIMITED should be a class constant somewhere.
    return ($this->cardinality > 1) || ($this->cardinality == FIELD_CARDINALITY_UNLIMITED);
  }

  /**
   * Implements FieldInterface::isValidEntityType().
   *
   * @todo find better method name ("valid" does not qualify the field)
   */
  public function isValidEntityType($entity_type) {
    return empty($this->entityTypes) || in_array($entity_type, $this->entityTypes);
  }

  /**
   * Implements FieldInterface::filterItems().
   */
  public function filterItems(array &$items) {
    foreach ($items as $delta => $item) {
      if ($this->handler()->itemIsEmpty($item)) {
        unset($items[$delta]);
      }
    }
    // Re-key the array to ensure consequent deltas.
    $items = array_values($items);
  }

  /**
   * Implements FieldInterface::hasData().
   */
  public function hasData() {
    // @todo This will fail id the field is being created (no id yet). Return FALSE if so.
    $query = new EntityFieldQuery();
    return (bool) $query
      ->fieldCondition($this)
      ->range(0, 1)
      ->count()
      // Neutralize the 'entity_field_access' query tag added by
      // field_sql_storage_field_storage_query(). The result cannot depend on the
      // access grants of the current user.
      ->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT')
      ->execute();
  }

  /**
   * Implements FieldInterface::schema().
   * @todo do we we really want this method on the Field class ? what about other handler methods ?
   */
  public function schema() {
    $schema = $this->handler()->schema();
    $schema += array('columns' => array(), 'indexes' => array(), 'foreign keys' => array());

    return $schema;
  }

  /**
   * Implements FieldInterface::columns().
   */
  public function columns() {
    $schema = $this->schema();

    return (array) $schema['columns'];
  }

  /**
   * Implements FieldInterface::storageDetails().
   */
  public function storageDetails() {
    if (!isset($this->storageDetails)) {
      $details = $this->storage()->getDetails($this);
      drupal_alter('field_storage_details', $details, $this->definition);

      $this->storageDetails = $details;
    }

    return $this->storageDetails;
  }

  /**
   * Implements FieldInterface::bundles().
   */
  public function bundles() {
    if (!$this->deleted) {
      $map = _field_info_field_cache()->getFieldMap();
      return isset($map[$this->name]) ? $map[$this->name] : array();
    }
    return array();
  }

  /**
   * Implements FieldInterface::access().
   */
  public function access($op, $entity_type, EntityInterface $entity = NULL, $account = NULL) {
    global $user;

    if (!isset($account)) {
      $account = $user;
    }

    foreach (module_implements('field_access') as $module) {
      $function = $module . '_field_access';
      $access = $function($op, $this, $entity_type, $entity, $account);
      if ($access === FALSE) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * @todo.
   */
  public function getSettings() {
    if (empty($this->defaultSettingsMerged)) {
      $this->settings += $this->getDefaultSettings();
      $this->defaultSettingsMerged = TRUE;
    }
    return $this->settings;
  }

  /**
   * @todo.
   */
  public function getSetting($key) {
    $settings = $this->getSettings();
    return isset($settings[$key]) ? $settings[$key] : NULL;
  }

  /**
   * @todo.
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    $this->defaultSettingsMerged = FALSE;

    return $this;
  }

  /**
   * @todo.
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;

    return $this;
  }

  /**
   * @todo.
   */
  public function getDefaultSettings() {
    $definition = $this->handler()->getDefinition();
    return $definition['settings'];
  }


  /**
   * Getters - deprectaed for now, we'll see how core standardizes.
   */

//  /**
//   * Implements FieldInterface::name().
//   */
//  public function name() {
//    return $this->name;
//  }
//
//  /**
//   * Implements FieldInterface::type().
//   */
//  public function type() {
//    return $this->type;
//  }
//
//  /**
//   * Implements FieldInterface::cardinality().
//   */
//  public function cardinality() {
//    return $this->cardinality;
//  }
//
//  /**
//   * Implements FieldInterface::isDeleted().
//   */
//  public function isDeleted() {
//    return $this->deleted;
//  }
//
//  /**
//   * Implements FieldInterface::isTranslatable().
//   */
//  public function isTranslatable() {
//    return $this->translatable;
//  }
//
//  /**
//   * Implements FieldInterface::isLocked().
//   */
//  public function isLocked() {
//    return $this->locked;
//  }
//
//  /**
//   * Implements FieldInterface::entityTypes().
//   */
//  public function entityTypes() {
//    return $this->entityTypes;
//  }
}
