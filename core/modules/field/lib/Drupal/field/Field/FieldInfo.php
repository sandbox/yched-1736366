<?php

/**
 * @file
 * Definition of Drupal\field\Field\FieldInfo.
 */

namespace Drupal\field\Field;

/**
 * Provides field and instance definitions for the current runtime environment.
 *
 * This class holds static and persistent caches of the collected data. The
 * methods retrieving all fields or all instances across all bundles do not read
 * from the caches (and do not populate the caches either). As such, they should
 * be used sparingly.
 *
 * The persistent cache uses one cache entry per bundle, storing both fields and
 * instances. Fields used in multiple bundles are thus replicated in several
 * cache entries (the static cache). Cache entries are loaded for bundles as a
 * whole, optimizing for the most common pattern of iterating over the instances
 * of a bundle (and then probably accessing each corresponding field) rather
 * than accessing a single one. The specific caching strategy and
 * interdependencies between fields and instances make the DrupalCacheArray
 * class unfit here.
 */
class FieldInfo {

  // @todo : There is currently no persistent cache for fields and instance
  // definitions. Current code does :
  // - 1 query on {field_config_instance} (field_read_instances()) by bundle
  // - 1 query on {field_config} (field_read_fields()) per field
  // This accounts for most of the performance hit compared to 8.x.
  // Revisit this with CMI integration.

  /**
   * Flag indicating a read or write operation on the static cache.
   */
  const CACHE_STATIC = 'static';

    /**
   * Flag indicating a read or write operation on the persistent cache.
   */
  const CACHE_PERSISTENT = 'persistent';

  /**
   * Lightweight map of fields across entity types and bundles.
   *
   * @var array
   */
  protected $fieldMap;

  /**
   * List of $field objects keyed by ID. Includes deleted fields.
   *
   * @var array
   */
  protected $fieldsById;

  /**
   * Mapping of field names to the ID of the corresponding non-deleted field.
   *
   * @var array
   */
  protected $fieldIdsByName;

  /**
   * List of $instance objects keyed by ID. Includes deleted instances.
   *
   * @var array
   */
  protected $instancesById;

  /**
   * Mapping of field names to the ID of the corresponding non-deleted instance.
   *
   * @var array
   */
  protected $instanceIdsByName;

  /**
   * Contents of bundles ($instance definitions and extra fields).
   *
   * @var array
   */
  protected $bundleInfo;

  /**
   * Constructs a FieldInfo object.
   */
  function __construct() {
    $this->init();
  }

  /**
   * Clears the static and persistent caches.
   */
  public function flush() {
    $this->init();
    cache('field')->deletePrefix('info_fields:');
  }

  /**
   * Initializes the static cache.
   */
  protected function init() {
    $this->fieldMap = array();
    $this->fieldsById = array();
    $this->fieldIdsByName = array();
    $this->instancesById = array();
    $this->instanceIdsByName = array();
    $this->bundleInfo = array();
  }

  /**
   * Returns a field object from a field name.
   *
   * This method only retrieves active, non-deleted fields.
   *
   * @param $field_name
   *   The field name.
   *
   * @return FieldInterface
   *   The field object, or NULL if no field was found.
   */
  public function getField($field_name) {
    if (isset($this->fieldIdsByName[$field_name])) {
      return $this->fieldsById[$this->fieldIdsByName[$field_name]];
    }

    // @todo what if we repeatedly get asked a non existent field ?
    if ($definition = field_read_field2(array('field_name' => $field_name))) {
      $field = Field::CreateFromConfig($definition);

      $field_id = $field->id();
      $this->fieldsById[$field_id] = $field;
      $this->fieldIdsByName[$field_name] = $field_id;

      return $field;
    }
  }

  /**
   * Returns a field object from a field ID.
   *
   * This method only retrieves active fields, deleted or not.
   *
   * @param $field_id
   *   The field ID.
   *
   * @return FieldInterface
   *   The field object, or NULL if no field was found.
   */
  public function getFieldById($field_id) {
    if (isset($this->fieldsById[$field_id])) {
      return $this->fieldsById[$field_id];
    }

    // @todo what if we repeatedly get asked a non existent field ?
    if ($definitions = field_read_fields2(array('id' => $field_id), array('include_deleted' => TRUE))) {
      $definition = current($definitions);
      $field = Field::CreateFromConfig($definition);

      $field_name = $field->name;
      $this->fieldsById[$field_id] = $field;
      if (!$field->deleted) {
        $this->fieldIdsByName[$field_name] = $field_id;
      }

      return $field;
    }
  }

  /**
   * Returns an instance object from a field name.
   *
   * This method only retrieves active, non-deleted instances.
   *
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle name.
   * @param $field_name
   *   The field name.
   *
   * @return FieldInterface
   *   The interface object, or NULL if no insatnce was found.
   */
  public function getInstance($entity_type, $bundle, $field_name) {
    if (isset($this->instanceIdsByName[$entity_type][$bundle][$field_name])) {
      return $this->instancesById[$this->instanceIdsByName[$entity_type][$bundle][$field_name]];
    }

    // @todo what if we repeatedly get asked a non existent instance ?
    if ($definition = field_read_instance($entity_type, $field_name, $bundle)) {
      $instance = FieldInstance::CreateFromConfig($definition);

      $instance_id = $instance->id();
      $this->instancesById[$instance_id] = $instance;
      $this->instanceIdsByName[$entity_type][$bundle][$field_name] = $instance_id;

      return $instance;
    }
  }

  /**
   * Returns an instance object from a field name.
   *
   * This method only retrieves active instances, deleted or not.
   *
   * @param $instance_id
   *   The instance ID.
   *
   * @return FieldInterface
   *   The interface object, or NULL if no insatnce was found.
   */
  public function getInstanceById($instance_id) {
    if (isset($this->instancesById[$instance_id])) {
      return $this->instancesById[$instance_id];
    }

    // @todo what if we repeatedly get asked a non existent instance ?
    if ($definitions = field_read_instances(array('id' => $instance_id), array('inclde_deleted' => TRUE))) {
      $definition = current($definitions);
      $instance = FieldInstance::CreateFromConfig($definition);

      $field_name = $definition['field_name'];
      $entity_type = $instance->entityType;
      $bundle = $instance->bundle;
      $this->instancesById[$instance_id] = $instance;
      if (!$instance->deleted) {
        $this->instanceIdsByName[$entity_type][$bundle][$field_name] = $instance_id;
      }

      return $instance;
    }
  }

  /**
   * Collects a lightweight map of fields across bundles.
   *
   * @return
   *   An array keyed by field name. Each value is an array with entity
   *   types as keys and the array of bundle names as values.
   */
  public function getFieldMap() {
    if ($map = $this->fieldMap) {
      return $map;
    }

    if ($cached = cache('field')->get('field_map')) {
      $map = $cached->data;
      $this->fieldMap = $map;
      return $map;
    }

    $map = array();

    $query = db_select('field_config_instance', 'fci');
    $query->join('field_config', 'fc', 'fc.id = fci.field_id');
    $query->fields('fci', array('field_name', 'entity_type', 'bundle'))
      ->condition('fc.active', 1)
      ->condition('fc.storage_active', 1)
      ->condition('fc.deleted', 0)
      ->condition('fci.deleted', 0);
    foreach ($query->execute() as $row) {
      $map[$row->field_name][$row->entity_type][] = $row->bundle;
    }

    $this->fieldMap = $map;
    cache('field')->set('field_map', $map);

    return $map;
  }

  /**
   * Returns all active, non-deleted fields.
   *
   * This method does not read from nor populate the static and persistent
   * caches.
   *
   * @return
   *   An array of field definitions, keyed by field name.
   */
  public function getFields() {
    $fields = array();
    foreach (field_read_fields2() as $definition) {
      $fields[$definition['field_name']] = Field::CreateFromConfig($definition);
    }
    return $fields;
  }

  /**
   * Returns all active fields, including deleted ones.
   *
   * This method does not read from nor populate the static and persistent
   * caches.
   *
   * @return
   *   An array of field definitions, keyed by field ID.
   */
  public function getFieldsById() {
    $fields = array();
    foreach (field_read_fields2(array(), array('include_deleted' => TRUE)) as $definition) {
      $fields[$definition['id']] = Field::CreateFromConfig($definition);;
    }
    return $fields;
  }

  /**
   * Retrieves all active, non-deleted instances definitions.
   *
   * This method does not read from nor populate the static and persistent
   * caches.
   *
   * @param $entity_type
   *   (optional) The entity type.
   *
   * @return
   *   If $entity_type is not set, all instances keyed by entity type and bundle
   *   name. If $entity_type is set, all instances for that entity type, keyed
   *   by bundle name.
   */
  public function getInstances($entity_type = NULL) {
    $instances = array();

    // We need the field type for each instance. Fetch that directly from the
    // database rather than loading all field definitions in memory with
    // field_read_fields().
    $field_types = db_query("SELECT field_name, type FROM {field_config} WHERE active = 1 AND storage_active = 1 AND deleted = 0")->fetchAllKeyed();

    // Collect and prepare instances.
    $params = isset($entity_type) ? array('entity_type' => $entity_type) : array();
    foreach (field_read_instances($params) as $definition) {
      $instances[$definition['entity_type']][$definition['bundle']][$definition['field_name']] = FieldInstance::CreateFromConfig($definition);
    }

    if (isset($entity_type)) {
      return isset($instances[$entity_type]) ? $instances[$entity_type] : array();
    }
    else {
      return $instances;
    }
  }

  /**
   * Retrieves the extra fields for a bundle.
   * @todo rename... / do we want to keep this ?
   *
   * The function also populates the correpsonding field definitions in the
   * static cache.
   *
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle name.
   *
   * @return
   *   An array with the following key/value pairs:
   *   - instances: The list of instance definitions.
   *   - extra_fields: The list extra fields.
   */
  public function getBundleInfo($entity_type, $bundle) {
    if ($info = $this->cacheGetBundleInfo(self::CACHE_STATIC, $entity_type, $bundle)) {
      return $info;
    }

    $info = array(
      'instances' => array(),
      'extra_fields' => array(),
    );

    // This iterates on non-deleted instances, so deleted fields are kept
    // out of the persistent caches.
    $params = array('entity_type' => $entity_type, 'bundle' => $bundle);
    foreach (field_read_instances($params) as $definition) {
      $info['instances'][$definition['field_name']] = FieldInstance::CreateFromConfig($definition);
    }

    // Populate 'extra_fields'. Note: given the current shape of
    // hook_field_extra_fields(), we have no other way than collecting extra
    // fields on all bundles.
    $extra = module_invoke_all('field_extra_fields');
    drupal_alter('field_extra_fields', $extra);
    // Merge in saved settings.
    if (isset($extra[$entity_type][$bundle])) {
      $info['extra_fields'] = $this->prepareExtraFields($extra[$entity_type][$bundle], $entity_type, $bundle);
    }

    // The method might get called on invalid entity types or bundles, for
    // which we do not want to pollute our caches. However, there might be
    // edge cases where the entity type and bundle are valid but still unknown
    // by a stale entity_get_info(). Thus on unknown entity types or bundles,
    // we do check the corresponding field instances, but do not cache the
    // result.
    $entity_info = entity_get_info($entity_type);
    if (isset($entity_info['bundles'][$bundle])) {
      $this->cacheSetBundleInfo(self::CACHE_STATIC, $entity_type, $bundle, $info);
    }

    return $info;
  }

  /**
   * Prepares 'extra fields' for the current run-time context.
   *
   * @param $extra_fields
   *   The array of extra fields, as collected in hook_field_extra_fields().
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle name.
   *
   * @return
   *   The list of extra fields completed for the current runtime context.
   */
  public function prepareExtraFields($extra_fields, $entity_type, $bundle) {
    $entity_type_info = entity_get_info($entity_type);
    $bundle_settings = field_bundle_settings($entity_type, $bundle);
    $extra_fields += array('form' => array(), 'display' => array());

    $result = array();
    // Extra fields in forms.
    foreach ($extra_fields['form'] as $name => $field_data) {
      $settings = isset($bundle_settings['extra_fields']['form'][$name]) ? $bundle_settings['extra_fields']['form'][$name] : array();
      if (isset($settings['weight'])) {
        $field_data['weight'] = $settings['weight'];
      }
      $result['form'][$name] = $field_data;
    }

    // Extra fields in displayed entities.
    $data = $extra_fields['display'];
    foreach ($extra_fields['display'] as $name => $field_data) {
      $settings = isset($bundle_settings['extra_fields']['display'][$name]) ? $bundle_settings['extra_fields']['display'][$name] : array();
      $view_modes = array_merge(array('default'), array_keys($entity_type_info['view modes']));
      foreach ($view_modes as $view_mode) {
        if (isset($settings[$view_mode])) {
          $field_data['display'][$view_mode] = $settings[$view_mode];
        }
        else {
          $field_data['display'][$view_mode] = array(
            'weight' => $field_data['weight'],
            'visible' => isset($field_data['visible']) ? $field_data['visible'] : TRUE,
          );
        }
      }
      unset($field_data['weight']);
      unset($field_data['visible']);
      $result['display'][$name] = $field_data;
    }

    return $result;
  }

  /**
   * Retrieves the contents of a bundle from the static cache.
   *
   * @param $cache
   *   The type of cache. Either self::CACHE_STATIC or self::CACHE_PERSISTENT.
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle name.
   *
   * @return
   *   The contents of the bundle, as returned by getBundleInfo(), if present in
   *   the static cache, else NULL.
   */
  protected function cacheGetBundleInfo($cache, $entity_type, $bundle) {
    switch ($cache) {
      case self::CACHE_STATIC:
        if (isset($this->bundleInfo[$entity_type][$bundle])) {
          return $this->bundleInfo[$entity_type][$bundle];
        }
        break;

      case self::CACHE_PERSISTENT:
        if ($cached = cache('field')->get("bundle_info:$entity_type:$bundle")) {
          return $cached->data;
        }
        break;
    }
  }

  /**
   * Stores the contents of a bundle in the static cache.
   *
   * @param $cache
   *   The type of cache. Either self::CACHE_STATIC or self::CACHE_PERSISTENT.
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The bundle name.
   * @param $info
   *   The contents of the bundle, as returned by getBundleInfo()
   */
  protected function cacheSetBundleInfo($cache, $entity_type, $bundle, $info) {
    switch ($cache) {
      case self::CACHE_STATIC:
        $this->bundleInfo[$entity_type][$bundle] = $info;
        break;

      case self::CACHE_PERSISTENT:
        cache('field')->set("bundle_info:$entity_type:$bundle", $info);
        break;
    }
  }
}
