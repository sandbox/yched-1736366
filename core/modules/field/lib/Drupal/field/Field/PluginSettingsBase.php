<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\PluginSettingsBase.
 */

namespace Drupal\field\Field;
use Drupal\Component\Plugin\PluginBase;

/**
 * Base class various Field API plugins.
 */
abstract class PluginSettingsBase extends PluginBase {

  protected $settings = array();

  protected $defaultSettingsMerged = FALSE;

  /**
   * @todo.
   */
  public function getSettings() {
    if (!$this->defaultSettingsMerged) {
      $this->mergeDefaults();
    }
    return $this->settings;
  }

  /**
   * @todo.
   */
  public function getSetting($key) {
    if (!$this->defaultSettingsMerged) {
      $this->mergeDefaults();
    }
    return isset($this->settings[$key]) ? $this->settings[$key] : NULL;
  }

  /**
   * @todo.
   */
  protected function mergeDefaults() {
    $this->settings += $this->getDefaultSettings();
    $this->defaultSettingsMerged = TRUE;
  }

  public function getDefaultSettings() {
    $definition = $this->getDefinition();
    return $definition['settings'];
  }
  
  /**
   * @todo.
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    $this->defaultSettingsMerged = FALSE;
    return $this;
  }

  /**
   * @todo.
   */
  public function setSetting($key, $value) {
    $this->settings[$key] = $value;
    return $this;
  }

}
