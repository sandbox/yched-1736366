<?php

/**
 * Definition of Drupal\field\Field\FieldInstanceInterface.
 */

namespace Drupal\field\Field;
use Drupal\field\Field\FieldType\FieldHandlerInterface;
use Drupal\field\Field\Formatter\FormatterInterface;
use Drupal\entity\EntityInterface;

/**
 * Interface definition for field instance objects.
 */
interface FieldInstanceInterface {

  /**
   * Creates an object from a stored definition.
   *
   * @param $definition
   *   The instance definition.
   *
   * @return FieldInstanceInterface
   *   The field instance object.
   */
  public static function CreateFromConfig(array $definition);

  /**
   * Saves a field instance definition to the configuration layer.
   *
   * @return FieldInstanceInterface
   *   The saved instance object.
   */
  public function save();

  /**
   * Marks a field instance and its data for deletion.
   *
   * @return FieldInstanceInterface
   *   The instance object.
   */
  public function delete();

  /**
   * Returns the field type handler.
   *
   * @return FieldHandlerInterface
   *   The field handler.
   */
  public function handler();

  /**
   * Returns the widget controller.
   *
   * @return WidgetInterface
   *   The widget plugin.
   */
  public function widget();

    /**
   * Returns the widget bridge object.
   *
   * @return Drupal\field\Field\FieldWidgetBridge
   *   The widget bridge..
   */
  public function widgetBridge();

  /**
   * @todo
   */
  public function getWidgetConfig();

  /**
   * @todo
   *
   * @return FieldInstanceInterface
   */
  public function setWidgetConfig(array $properties);

  /**
   * Returns a formatter handler.
   *
   * @param $display
   *   Can be either:
   *   - The name of a view mode.
   *     @todo : from_field_view_field() : "If no display settings are found for
   *     the view mode, the settings for the 'default' view mode will be used." ??
   *   - An array of display properties, as accepted by setDisplayConfig().
   *
   * @return FormatterInterface
   *   The formatter handler (or NULL if the field is hidden)
   */
  public function formatter($display);

  /**
   * @todo
   */
  public function getDisplayConfig($view_mode);

  /**
   * @todo
   */
  public function setDisplayConfig($view_mode, array $properties);

  /**
   * Returns the field associated to the instance.
   *
   * @return FieldInterface
   *   The field associated to the instance.
   */
  public function field();

  /**
   * Returns the instance ID.
   *
   * @return
   *   The instance ID.
   */
  public function id();

  /**
   * Returns the field ID.
   *
   * @return
   *   The field ID.
   */
  public function fieldId();

  /**
   * @todo
   */
  public function getSettings();

  /**
   * @todo
   */
  public function getSetting($key);

  /**
   * @todo
   *
   * @return FieldInstanceInterface
   */
  public function setSettings(array $settings);

  /**
   * @todo
   *
   * @return FieldInstanceInterface
   */
  public function setSetting($key, $value);

  /**
   * Computes the default value for a field on an entity.
   *
   * @param $entity_type
   *   The type of $entity; e.g., 'node' or 'user'.
   * @param $entity
   *   The entity for the operation.
   * @param $langcode
   *   The field language to fill-in with the default value.
   */
  public function computeDefaultValue($entity_type, EntityInterface $entity, $langcode = NULL);



   /**
   * Getters - deprectaed for now, we'll see how core standardizes.
   */

//  /**
//   * Returns the entity type to which the instance is attached.
//   *
//   * @return
//   *   The entity type.
//   */
//  public function entityType();
//
//  /**
//   * Returns the entity type to which the instance is attached.
//   *
//   * @return
//   *   The entity type.
//   */
//  public function bundle();
//
//  /**
//   * Returns the instance label.
//   *
//   * @return
//   *   The label.
//   */
//  public function label();
//
//  /**
//   * Returns the instance description.
//   *
//   * @return
//   *   The description.
//   */
//  public function description();
//
//  /**
//   * Returns whether a field value is required in entities.
//   *
//   * @return
//   *   TRUE if the instance is required, FALSE if not.
//   */
//  public function isRequired();
//
//  /**
//   * Implements FieldInterface::isDeleted().
//   *
//   * @return
//   *   TRUE if the instance is deleted, FALSE if not.
//   */
//  public function isDeleted();
//
//  /**
//   * Returns the instance (hardcoded) default value.
//   *
//   * Warning: this returns the instance property. To compute the actual default
//   * value, use the computeDefaultValue() method.
//   */
//  public function defaultValue();
//
//  /**
//   * Returns the instance default value function.
//   *
//   * Warning: this returns the instance property. To compute the actual default
//   * value, use the computeDefaultValue() method.
//   */
//  public function defaultValueFunction();
}
