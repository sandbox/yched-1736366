<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\FieldWidgetBridge.
 */

namespace Drupal\field\Field;
use Drupal\field\Field\FieldInterface;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\field\Field\Widget\WidgetInterface;
use Drupal\entity\EntityInterface;

/**
 * Base class for 'Field widget' plugin implementations.
 */
class FieldWidgetBridge {

  /**
   * @var FieldInterface
   */
  protected $field;

  /**
   * @var FieldInstanceInterface
   */
  protected $instance;

  /**
   * @var WidgetInterface
   */
  protected $widget;

  public function __construct(FieldInstanceInterface $instance, WidgetInterface $widget, $weight) {
    $this->instance = $instance;
    $this->field = $instance->field();
    $this->widget = $widget;
    $this->weight = $weight;
  }

  /**
   * Creates a form element for a field.
   *
   * If the form element is not associated with an entity (i.e., $entity is
   * NULL), the 'default value', if present, is pre-populated. Also allows other
   * modules to alter the form element by implementing their own hooks.
   *
   * @param $entity_type
   *   The type of entity (for example 'node' or 'user') that the field belongs
   *   to.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity object that the field belongs to. This may be NULL if creating
   *   a form element with a default value.
   * @param Drupal\field\Field\FieldInstanceInterface $instance
   *   The field instance.
   * @param $langcode
   *   The language associated with the field.
   * @param $items
   *   An array of the field values. When creating a new entity this may be NULL
   *   or an empty array to use default values.
   * @param $form
   *   An array representing the form that the editing element will be attached
   *   to.
   * @param $form_state
   *   An array containing the current state of the form.
   * @param $get_delta
   *   Used to get only a specific delta value of a multiple value field.
   *
   * @return
   *  The form element array created for this field.
   *
   * @todo receives $instance (because of field_invoke_method()), but already has $this->field, $this->instance
   */
  public function form($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, array &$form, array &$form_state, $get_delta = NULL) {
    $field = $this->field;
    $field_name = $this->field->name;

    $parents = $form['#parents'];

    $addition = array(
      $field_name => array(),
    );

    // Populate widgets with default values when creating a new entity. Beware
    // that the method could be called with no entity, as when a UI module
    // creates a dummy form to set default values.
    if (empty($items) && (empty($entity) || $entity->isNew())) {
      $items = $instance->computeDefaultValue($entity_type, $entity, $langcode);
    }

    // Let modules alter the widget properties.
    $context = array(
      'entity_type' => $entity_type,
      'entity' => $entity,
      'field' => $field,
      'instance' => $instance,
      'default' => empty($entity),
    );
    drupal_alter(array('field_widget_properties', 'field_widget_properties_' . $entity_type), $this, $context);

    // Store field information in $form_state.
    if (!field_form_get_state($parents, $field_name, $langcode, $form_state)) {
      $field_state = array(
        'field' => $field,
        'instance' => $instance,
        'items_count' => count($items),
        'array_parents' => array(),
        'errors' => array(),
      );
      field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
    }

    // Collect widget elements.
    $elements = array();

    // If the widget is handling multiple values (e.g Options), or if we are
    // displaying an individual element, just get a single form element and make
    // it the $delta value.
    // @todo get rid of $get_delta ?
    $definition = $this->widget->getDefinition();
    if (isset($get_delta) || $definition['multiple_values']) {
      $delta = isset($get_delta) ? $get_delta : 0;
      $element = array(
        '#title' => check_plain($instance->label),
        '#description' => field_filter_xss($instance->description),
      );
      $element = $this->formSingleElement($entity, $items, $delta, $langcode, $element, $form, $form_state);

      if ($element) {
        if (isset($get_delta)) {
          // If we are processing a specific delta value for a field where the
          // field module handles multiples, set the delta in the result.
          $elements[$delta] = $element;
        }
        else {
          // For fields that handle their own processing, we cannot make
          // assumptions about how the field is structured, just merge in the
          // returned element.
          $elements = $element;
        }
      }
    }
    // If the widget does not handle multiple values itself, (and we are not
    // displaying an individual element), process the multiple value form.
    else {
      $elements = $this->formMultipleElements($entity, $items, $langcode, $form, $form_state);
    }

    // Also aid in theming of field widgets by rendering a classified
    // container.
    $addition[$field_name] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'field-type-' . drupal_html_class($field->type),
          'field-name-' . drupal_html_class($field_name),
          'field-widget-' . drupal_html_class($this->widget->getPluginId()),
        ),
      ),
      '#weight' => $this->weight,
    );

    // Populate the 'array_parents' information in $form_state['field'] after
    // the form is built, so that we catch changes in the form structure performed
    // in alter() hooks.
    $elements['#after_build'][] = 'field_form_element_after_build';
    $elements['#field_name'] = $field_name;
    $elements['#language'] = $langcode;
    $elements['#field_parents'] = $parents;

    $addition[$field_name] += array(
      '#tree' => TRUE,
      // The '#language' key can be used to access the field's form element
      // when $langcode is unknown.
      '#language' => $langcode,
      $langcode => $elements,
      '#access' => $field->access('edit', $entity_type, $entity),
    );

    return $addition;
  }

  /**
   * Special handling to create form elements for multiple values.
   *
   * Handles generic features for multiple fields:
   * - number of widgets
   * - AHAH-'add more' button
   * - table display and drag-n-drop value reordering
   */
  protected function formMultipleElements(EntityInterface $entity, array $items, $langcode, array &$form, array &$form_state) {
    $field = $this->field;
    $instance = $this->instance;
    $field_name = $field->name;

    $parents = $form['#parents'];

    // Determine the number of widgets to display.
    switch ($field->cardinality) {
      case FIELD_CARDINALITY_UNLIMITED:
        $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
        $max = $field_state['items_count'];
        break;

      default:
        $max = $field->cardinality - 1;
        break;
    }

    $id_prefix = implode('-', array_merge($parents, array($field_name)));
    $wrapper_id = drupal_html_id($id_prefix . '-add-more-wrapper');

    $title = check_plain($instance->label);
    $description = field_filter_xss($instance->description);

    $elements = array();

    for ($delta = 0; $delta <= $max; $delta++) {
      // @todo Should we go through formMultipleElements() even if cardinality is 1 ?
      $multiple = $field->isMultiple();

      // For multiple fields, title and description are handled by the wrapping
      // table.
      $element = array(
        '#title' => $multiple ? '' : $title,
        '#description' => $multiple ? '' : $description,
      );
      $element = $this->formSingleElement($entity, $items, $delta, $langcode, $element, $form, $form_state);

      if ($element) {
        // Input field for the delta (drag-n-drop reordering).
        if ($multiple) {
          // We name the element '_weight' to avoid clashing with elements
          // defined by widget.
          $element['_weight'] = array(
            '#type' => 'weight',
            '#title' => t('Weight for row @number', array('@number' => $delta + 1)),
            '#title_display' => 'invisible',
            // Note: this 'delta' is the FAPI #type 'weight' element's property.
            '#delta' => $max,
            '#default_value' => isset($items[$delta]['_weight']) ? $items[$delta]['_weight'] : $delta,
            '#weight' => 100,
          );
        }

        $elements[$delta] = $element;
      }
    }

    if ($elements) {
      $elements += array(
        '#theme' => 'field_multiple_value_form',
        '#field_name' => $field->name,
        '#cardinality' => $field->cardinality,
        '#required' => $instance->required,
        '#title' => $title,
        '#description' => $description,
        '#prefix' => '<div id="' . $wrapper_id . '">',
        '#suffix' => '</div>',
        '#max_delta' => $max,
      );

      // Add 'add more' button, if not working with a programmed form.
      if ($field->cardinality == FIELD_CARDINALITY_UNLIMITED && empty($form_state['programmed'])) {
        $elements['add_more'] = array(
          '#type' => 'submit',
          '#name' => strtr($id_prefix, '-', '_') . '_add_more',
          '#value' => t('Add another item'),
          '#attributes' => array('class' => array('field-add-more-submit')),
          '#limit_validation_errors' => array(array_merge($parents, array($field_name, $langcode))),
          '#submit' => array('field_add_more_submit'),
          '#ajax' => array(
            'callback' => 'field_add_more_js',
            'wrapper' => $wrapper_id,
            'effect' => 'fade',
          ),
        );
      }
    }

    return $elements;
  }

  /**
   * @todo
   */
  protected function formSingleElement(EntityInterface $entity, array $items, $delta, $langcode, array $element, array &$form, array &$form_state) {
    $instance = $this->instance;
    $field = $this->field;

    $element += array(
      '#entity_type' => $instance->entityType,
      '#bundle' => $instance->bundle,
      '#entity' => $entity,
      '#field_name' => $field->name,
      '#language' => $langcode,
      '#field_parents' => $form['#parents'],
      '#columns' => array_keys($field->columns()),
      // Only the first widget should be required.
      '#required' => $delta == 0 && $instance->required,
      '#delta' => $delta,
      '#weight' => $delta,
    );

    $element = $this->widget->formElement($items, $delta, $element, $langcode, $form, $form_state);

    if ($element) {
      // Allow modules to alter the field widget form element.
      $context = array(
        'form' => $form,
        'instance' => $instance,
        'langcode' => $langcode,
        'items' => $items,
        'delta' => $delta,
        'default' => empty($entity),
      );
      drupal_alter(array('field_widget_form', 'field_widget_' . $this->widget->getPluginId() . '_form'), $element, $form_state, $context);
    }

    return $element;
  }

  /**
   * Displays field-level validation errors.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param Drupal\entity\EntityInterface $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated to $items.
   * @param $items
   *   The field values.
   * @param $form
   *   The form structure where field elements are attached to. This might be a
   *   full form structure, or a sub-element of a larger form.
   * @param $form_state
   *   The form state.
   */
  public function flagErrors($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, array $form, array &$form_state) {
    $field_name = $this->field->name;

    $field_state = field_form_get_state($form['#parents'], $field_name, $langcode, $form_state);

    if (!empty($field_state['errors'])) {
      // Locate the correct element in the the form.
      $element = drupal_array_get_nested_value($form_state['complete_form'], $field_state['array_parents']);

      // Only set errors if the element is accessible.
      if (!isset($element['#access']) || $element['#access']) {
        $definition = $this->widget->getDefinition();
        $is_multiple = $definition['multiple_values'];

        foreach ($field_state['errors'] as $delta => $delta_errors) {
          // For single-value widgets, pass errors by delta.
          // For a multiple-value widget, pass all errors to the main widget.
          $delta_element = $is_multiple ? $element : $element[$delta];
          foreach ($delta_errors as $error) {
            $error_element = $this->widget->errorElement($delta_element, $error, $form, $form_state);
            form_error($error_element, $error['message']);
          }
        }
        // Reinitialize the errors list for the next submit.
        $field_state['errors'] = array();
        field_form_set_state($form['#parents'], $field_name, $langcode, $form_state, $field_state);
      }
    }
  }

  /**
   * Extracts field values from submitted form values.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param $entity
   *   The entity for the operation.
   * @param $instance
   *   The FieldInstance object.
   * @param $langcode
   *   The language associated to $items.
   * @param $items
   *   The field values. This parameter is altered by reference to receive the
   *   incoming form values.
   * @param $form
   *   The form structure where field elements are attached to. This might be a
   *   full form structure, or a sub-element of a larger form.
   * @param $form_state
   *   The form state.
   */
  public function extractFormValues($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items, array $form, array &$form_state) {
    $field_name = $this->field->name;

    $items = array();

    // Extract the values from $form_state['values'].
    $path = array_merge($form['#parents'], array($field_name, $langcode));
    $key_exists = NULL;
    $values = drupal_array_get_nested_value($form_state['values'], $path, $key_exists);

    if ($key_exists) {
      // Remove the 'value' of the 'add more' button.
      unset($values['add_more']);

      // Let the widget turn the submitted values into actual field values.
      // Make sure the '_weight' entries are persisted in the process.
      $weights = array();
      if (isset($values[0]['_weight'])) {
        foreach ($values as $delta => $value) {
          $weights[$delta] = $value['_weight'];
        }
      }
      $items = $this->widget->fieldValuesFromFormValues($values, $form, $form_state);
      if ($weights) {
        foreach ($items as $delta => $item) {
          $items[$delta]['_weight'] = $weights[$delta];
        }
      }
    }

    // Save the extracted $items in $form_state, so that we do not have to
    // compute them again in submit().
    // @todo Why don't we just use form_set_value() ?
    $field_state = field_form_get_state($form['#parents'], $field_name, $langcode, $form_state);
    $field_state['items'] = $items;
    field_form_set_state($form['#parents'], $field_name, $langcode, $form_state, $field_state);
  }

  /**
   * @todo merge with extractFormValues() ?
   */
  public function submit($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items, array $form, array &$form_state) {
    $field_name = $this->field->name;

    // Get the $items saved in $form_state during extractFormValues().
    $field_state = field_form_get_state($form['#parents'], $field_name, $langcode, $form_state);
    $items = $field_state['items'];

    // Account for drag-n-drop reordering.
    $this->sortItems($items);
  }

  /**
   * Sorts submitted field values according to drag-n-drop reordering.
   *
   * @param $items
   *   The field values.
   */
  protected function sortItems(array &$items) {
    if ($this->field->isMultiple() && isset($items[0]['_weight'])) {
      usort($items, function ($a, $b) {
        $a_weight = (is_array($a) ? $a['_weight'] : 0);
        $b_weight = (is_array($b) ? $b['_weight'] : 0);
        return $a_weight - $b_weight;
      });
      // Remove the '_weight' entires.
      foreach ($items as $delta => &$item) {
        if (is_array($item)) {
          unset($item['_weight']);
        }
      }
    }
  }
}
