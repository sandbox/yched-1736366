<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\Widget\BaseWidget.
 */

namespace Drupal\field\Field\Widget;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\field\Field\PluginSettingsBase;
use Drupal\Component\Plugin\Discovery\DiscoveryInterface;

/**
 * Base class for 'Field widget' plugin implementations.
 */
abstract class BaseWidget extends PluginSettingsBase implements WidgetInterface {

  /**
   * @var Drupal\field\Field\FieldInterface
   */
  protected $field;

  /**
   * @var Drupal\field\Field\FieldInstanceInterface
   */
  protected $instance;

  public function __construct($plugin_id, DiscoveryInterface $discovery, FieldInstanceInterface $instance, array $settings) {
    parent::__construct(array(), $plugin_id, $discovery);

    $this->instance = $instance;
    $this->field = $this->instance->field();
    $this->settings = $settings;
  }

  /**
   * Implements WidgetInterface::errorElement().
   */
  public function errorElement(array $element, array $error, array $form, array &$form_state) {
    return $element;
  }

  /**
   * Implements WidgetInterface::fieldValuesFromFormValues()
   */
  public function fieldValuesFromFormValues($values, $form, &$form_state) {
    return $values;
  }
}
