<?php

/**
 * @file
 *
 * Definition of Drupal\field\Field\Storage\BaseFieldStorage.
 */

namespace Drupal\field\Field\Storage;
use Drupal\field\Field\FieldInterface;
use Drupal\field\Field\PluginSettingsBase;
use Drupal\Component\Plugin\Discovery\DiscoveryInterface;

/**
 * Base class for 'Field storage' plugin implementations.
 */
abstract class BaseFieldStorage extends PluginSettingsBase implements FieldStorageInterface {

  public function __construct($plugin_id, DiscoveryInterface $discovery, array $settings) {
    parent::__construct(array(), $plugin_id, $discovery);

    $this->settings = $settings;
  }

  /**
   * Implements FieldStorageInterface::createField().
   */
  public function GetDetails(FieldInterface $field) { }

  /**
   * Implements FieldStorageInterface::createField().
   */
  public function createField(FieldInterface $field) { }

  /**
   * Implements FieldStorageInterface::updateField().
   */
  public function updateField(FieldInterface $field, FieldInterface $prior_field, $has_data) { }
}
