<?php

/**
 * Definition of Drupal\field\Field\Storage\FieldStorageInterface.
 */

namespace Drupal\field\Field\Storage;
use Drupal\field\Field\FieldInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface definition for 'Field storage' plugin implementations.
 */
interface FieldStorageInterface extends PluginInspectionInterface {

  /**
   * @todo
   */
  public function getSettings();

  /**
   * @todo
   */
  public function getSetting($key);

  /**
   * @todo
   */
  public function setSettings(array $settings);

  /**
   * @todo
   */
  public function setSetting($key, $value);

  /**
   * @todo from hook_field_storage_details().
   */
  public function getDetails(FieldInterface $field);

  /**
   * @todo from hook_field_storage_create().
   */
  public function createField(FieldInterface $field);

  /**
   * @todo from hook_field_storage_updtate().
   */
  public function updateField(FieldInterface $field, FieldInterface $prior_field, $has_data);

}
