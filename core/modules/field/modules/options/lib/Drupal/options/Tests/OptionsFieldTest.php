<?php

/**
 * @file
 * Definition of Drupal\options\Tests\OptionsFieldTest.
 */

namespace Drupal\options\Tests;

use Drupal\field\Tests\FieldTestBase;
use Drupal\field\Field\FieldInterface;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\field\FieldException;

/**
 * Tests for the 'Options' field types.
 */
class OptionsFieldTest extends FieldTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('options', 'field_test');

  var $field_name;

  /**
   * @var FieldInterface
   */
  var $field;

  /**
   * @var FieldInstanceInterface
   */
  var $instance;


  public static function getInfo() {
    return array(
      'name' => 'Options field',
      'description' => 'Test the Options field type.',
      'group' => 'Field types',
    );
  }

  function setUp() {
    parent::setUp();

    $this->field_name = 'test_options';

    $this->field = field_create_field2($this->field_name, 'list_integer')
      ->setSettings(array(
        'allowed_values' => array(1 => 'One', 2 => 'Two', 3 => 'Three'),
      ))
      ->save();

    $this->instance = field_create_instance2('test_entity', 'test_bundle', $this->field_name)
      ->setWidgetConfig(array(
        'type' => 'options_buttons',
      ))
      ->save();
  }

  /**
   * Test that allowed values can be updated.
   */
  function testUpdateAllowedValues() {
    $field_name = $this->field_name;
    $field = $this->field;
    $instance = $this->instance;
    $langcode = LANGUAGE_NOT_SPECIFIED;

    // All three options appear.
    $entity = field_test_create_entity();
    $form = entity_get_form($entity);
    $this->assertTrue(!empty($form[$field_name][$langcode][1]), t('Option 1 exists'));
    $this->assertTrue(!empty($form[$field_name][$langcode][2]), t('Option 2 exists'));
    $this->assertTrue(!empty($form[$field_name][$langcode][3]), t('Option 3 exists'));

    // Use one of the values in an actual entity, and check that this value
    // cannot be removed from the list.
    $entity = field_test_create_entity();
    $entity->{$field_name}[$langcode][0] = array('value' => 1);
    field_test_entity_save($entity);
    try {
      $field
        ->setSetting('allowed_values', array(2 => 'Two'))
        ->save();
      $this->fail(t('Cannot update a list field to not include keys with existing data.'));
    }
    catch (FieldException $e) {
      $this->pass(t('Cannot update a list field to not include keys with existing data.'));
    }
    // Empty the value, so that we can actually remove the option.
    $entity->{$field_name}[$langcode] = array();
    field_test_entity_save($entity);

    // Removed options do not appear.
    $field
      ->setSetting('allowed_values', array(2 => 'Two'))
      ->save();
    $entity = field_test_create_entity();
    $form = entity_get_form($entity);
    $this->assertTrue(empty($form[$field_name][$langcode][1]), t('Option 1 does not exist'));
    $this->assertTrue(!empty($form[$field_name][$langcode][2]), t('Option 2 exists'));
    $this->assertTrue(empty($form[$field_name][$langcode][3]), t('Option 3 does not exist'));

    // Completely new options appear.
    $field
      ->setSetting('allowed_values', array(10 => 'Update', 20 => 'Twenty'))
      ->save();
    $form = entity_get_form($entity);
    $this->assertTrue(empty($form[$field_name][$langcode][1]), t('Option 1 does not exist'));
    $this->assertTrue(empty($form[$field_name][$langcode][2]), t('Option 2 does not exist'));
    $this->assertTrue(empty($form[$field_name][$langcode][3]), t('Option 3 does not exist'));
    $this->assertTrue(!empty($form[$field_name][$langcode][10]), t('Option 10 exists'));
    $this->assertTrue(!empty($form[$field_name][$langcode][20]), t('Option 20 exists'));

    // Options are reset when a new field with the same name is created.
    $field->delete();

    $field = field_create_field2($field_name, 'list_integer')
      ->setSettings(array(
        'allowed_values' => array(1 => 'One', 2 => 'Two', 3 => 'Three'),
      ))
      ->save();
    $instance = field_create_instance2('test_entity', 'test_bundle', $this->field_name)
      ->setWidgetConfig(array(
        'type' => 'options_buttons',
      ))
      ->save();
    $entity = field_test_create_entity();
    $form = entity_get_form($entity);
    $this->assertTrue(!empty($form[$field_name][$langcode][1]), t('Option 1 exists'));
    $this->assertTrue(!empty($form[$field_name][$langcode][2]), t('Option 2 exists'));
    $this->assertTrue(!empty($form[$field_name][$langcode][3]), t('Option 3 exists'));
  }
}
