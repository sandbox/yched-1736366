<?php

/**
 * @file
 *
 * Definition of Drupal\options\Field\Widget\OptionsOnOffWidget.
 */

namespace Drupal\options\Field\Widget;

/**
 * Plugin implementation of the 'options_onoff' widget.
 */
class OptionsOnOffWidget extends BaseOptionsWidget {

  /**
   * Implements WidgetInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['display_label'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use field label instead of the "On value" as label'),
      '#default_value' => $this->getSetting('display_label'),
      '#weight' => -1,
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $element = parent::formElement($items, $delta, $element, $langcode, $form, $form_state);

    $keys = array_keys($this->options);
    $off_value = array_shift($keys);
    $on_value = array_shift($keys);
    $element += array(
      '#type' => 'checkbox',
      '#default_value' => (isset($this->selected_options[0]) && $this->selected_options[0] == $on_value) ? 1 : 0,
      '#on_value' => $on_value,
      '#off_value' => $off_value,
    );

    // Override the title from the incoming $element.
    if ($this->getSetting('display_label')) {
      $element['#title'] = $this->instance->label;
    }
    else {
      $element['#title'] = isset($this->options[$on_value]) ? $this->options[$on_value] : '';
    }

    return $element;
  }

  /**
   * Implements WidgetInterface::fieldValuesFromFormValues().
   */
  public function fieldValuesFromFormValues($values, $form, &$form_state) {
    $keys = array_keys($this->options);
    $off_value = array_shift($keys);
    $on_value = array_shift($keys);

    // Transform '0 / 1' into the 'on / off' values.
    return array($values[0] ? $on_value : $off_value);
  }
}
