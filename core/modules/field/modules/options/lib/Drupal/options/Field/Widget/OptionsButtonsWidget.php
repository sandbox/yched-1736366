<?php

/**
 * @file
 *
 * Definition of Drupal\options\Field\Widget\OptionsButtonsWidget.
 */

namespace Drupal\options\Field\Widget;

/**
 * Plugin implementation of the 'options_buttons' widget.
 */
class OptionsButtonsWidget extends BaseOptionsWidget {

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $element = parent::formElement($items, $delta, $element, $langcode, $form, $form_state);

    // If required and there is one single option, preselect it.
    if ($this->required && count($this->options) == 1) {
      reset($this->options);
      $this->selected_options = array(key($this->options));
    }

    if ($this->field->isMultiple()) {
      $element += array(
        '#type' => 'checkboxes',
        '#default_value' => $this->selected_options,
        '#options' => $this->options,
      );
    }
    else {
      $element += array(
        '#type' => 'radios',
        // Radio buttons need a scalar value. Take the first default value, or
        // default to NULL so that the form element is properly recognized as
        // not having a default value
        '#default_value' => $this->selected_options ? reset($this->selected_options) : NULL,
        '#options' => $this->options,
      );
    }

    return $element;
  }

  /**
   * Overrides OptionsWidgetBase::prepareOptions().
   */
  protected function emptyOption() {
    if (!$this->required && !$this->field->isMultiple()) {
      $empty_option = 'option_none';
    }
  }
}
