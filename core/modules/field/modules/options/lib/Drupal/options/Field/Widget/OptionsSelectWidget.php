<?php

/**
 * @file
 *
 * Definition of Drupal\options\Field\Widget\OptionsSelectWidget.
 */

namespace Drupal\options\Field\Widget;

/**
 * Plugin implementation of the 'options_select' widget.
 */
class OptionsSelectWidget extends BaseOptionsWidget {

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $element = parent::formElement($items, $delta, $element, $langcode, $form, $form_state);

    $element += array(
      '#type' => 'select',
      '#default_value' => $this->selected_options,
      // Do not display a 'multiple' select box if there is only one option.
      '#multiple' => $this->field->isMultiple() && count($this->options) > 1,
      '#options' => $this->options,
    );

    return $element;
  }

  /**
   * Overrides OptionsWidgetBase::prepareOptionLabel().
   */
  static protected function prepareOptionLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = strip_tags($label);
  }

  /**
   * Overrides OptionsWidgetBase::supportsGroups().
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * Overrides OptionsWidgetBase::emptyOption().
   */
  protected function emptyOption() {
    if ($this->field->isMultiple()) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return self::OPTIONS_EMPTY_NONE;
      }
    }
    else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return self::OPTIONS_EMPTY_NONE;
      }
      if (!$this->has_value) {
        return self::OPTIONS_EMPTY_SELECT;
      }
    }
  }
}
