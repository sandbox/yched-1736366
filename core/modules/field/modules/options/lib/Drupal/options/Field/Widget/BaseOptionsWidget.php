<?php

/**
 * @file
 *
 * Definition of Drupal\options\Field\Widget\BaseOptionsWidget.
 */

namespace Drupal\options\Field\Widget;
use Drupal\field\Field\Widget\BaseWidget;

/**
 * Base class for the 'options_*' widgets.
 */
abstract class BaseOptionsWidget extends BaseWidget {

  /**
   * Identifies a 'None' option.
   */
  const OPTIONS_EMPTY_NONE = 'option_none';

  /**
   * Identifies a 'Select a value' option.
   */
  const OPTIONS_EMPTY_SELECT = 'option_select';

  /**
   * Abstract over the actual field columns, to allow different field types to
   * reuse those widgets.
   */
  protected $column;

  function __construct(array $config, Context $context) {
    parent::__construct($config, $context);

    $this->column = key($this->field->columns());
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $field = $this->field;
    $instance = $this->instance;

    // Prepare some properties for the child methods to build the actual form
    // element.
    $this->required = $element['#required'];
    $this->has_value = isset($items[0][$this->column]);

    // Get the list of options from the field type module, and sanitize them.
    $entity_type = $element['#entity_type'];
    $entity = $element['#entity'];
    // @todo ho do we get the field type module ??
    $this->options = (array) module_invoke($field->definition['module'], 'options_list', $field->definition, $instance->definition, $entity_type, $entity);
    array_walk_recursive($this->options, array($this, 'prepareOptionLabel'));

    // Options might be nested ("optgroups"). Assemble a flat version as well.
    $this->options_flat = $this->flattenOptions($this->options);

    // If the widget does not support nested options, keep the flat list.
    if (!$this->supportsGroups()) {
      $this->options = $this->options_flat;
    }

    // Add empty option if the widget needs one.
    if ($empty_option = $this->emptyOption()) {
      $label = theme('options_none', array('instance' => $instance, 'option' => $empty_option));
      $this->options = array('_none' => $label) + $this->options;
    }

    // Put current field values in shape.
    $this->selected_options = $this->formOptionsFromItems($items);

    // Add our custom validator.
    $element['#element_validate'][] = array(get_class($this), 'formElementValidate');

    // The rest of the $element is built by child method implementations.

    return $element;
  }

  /**
   * Form element validation handler for options element.
   */
  static public function formElementValidate(array $element, array &$form_state) {
    // @todo could this be handled by field-level validation ?
    if ($element['#required'] && $element['#value'] == '_none') {
      form_error($element, t('!name field is required.', array('!name' => $element['#title'])));
    }
  }

  /**
   * Sanitizes a string label to display as an option.
   */
  static protected function prepareOptionLabel(&$label) {
    // Allow a limited set of HTML tags.
    $label = field_filter_xss($label);
  }

  /**
   * Returns the empty option to add to the list of options, if any.
   *
   * @return
   *   Either OptionsWidgetBase::OPTIONS_EMPTY_NONE or
   *   OptionsWidgetBase::OPTIONS_EMPTY_SELECT.
   */
  protected function emptyOption() { }

  /**
   * Indicates whether the widgets support optgroups.
   *
   * @return
   *   TRUE if the widget supports optgroups, FALSE otherwise.
   */
  protected function supportsGroups() {
    return FALSE;
  }

  /**
   * Transforms stored field values into the format the widgets need.
   */
  protected function formOptionsFromItems(array $items) {
    $options = array();
    foreach ($items as $item) {
      $value = $item[$this->column];
      // Keep the value if it actually is in the list of options (nneds to be
      // checked against the flat list).
      if (isset($this->options_flat[$value])) {
        $options[] = $value;
      }
    }

    return $options;
  }

  /**
   * Implements WidgetInterface::fieldValuesFromFormValues().
   */
  public function fieldValuesFromFormValues($values, $form, &$form_state) {
    // Filter out the 'none' option. Use a strict comparison, because
    // 0 == 'any string'.
    $index = array_search('_none', $values, TRUE);
    if ($index !== FALSE) {
      unset($values[$index]);
    }

    // Transpose selections from field => delta to delta => field.
    $items = array();
    foreach ($values as $value) {
      $items[] = array($this->column => $value);
    }

    return $items;
  }

  /**
   * Flattens an array of allowed values.
   *
   * @param $array
   *   A single or multidimensional array.
   *
   * @return
   *   A flattened array.
   */
  static protected function flattenOptions(array $array) {
    $result = array();
    array_walk_recursive($array, function($a, $b) use (&$result) { $result[$b] = $a; });
    return $result;
  }
}
