<?php

namespace Drupal\field_sql_storage\Field\Storage;
use Drupal\field\Field\Storage\BaseFieldStorage;
use Drupal\field\Field\FieldInterface;
use Drupal\Core\Database\Database;

/**
 * @file
 *
 * Definition of Drupal\field_sql_storage\Field\Storage\SqlStorage.
 */

/**
 * Plugin implementation of the 'field_sql_storage' field storage type.
 */
class SqlStorage extends BaseFieldStorage {

  /**
   * Implements FieldStorageInterface::getDetails().
   */
  function getDetails(FieldInterface $field) {
    $details = array();
    $columns = $field->columns();
    if (!empty($columns)) {
      // Add field columns.
      foreach ($columns as $column_name => $attributes) {
        $real_name = $this->columnName($field, $column_name);
        $columns[$column_name] = $real_name;
      }
      return array(
        'sql' => array(
          FIELD_LOAD_CURRENT => array(
            $this->tableName($field, FIELD_LOAD_CURRENT) => $columns,
          ),
          FIELD_LOAD_REVISION => array(
            $this->tableName($field, FIELD_LOAD_REVISION) => $columns,
          ),
        ),
      );
    }
  }

  /**
   * Implements FieldStorageInterface::createField().
   */
  public function createField(FieldInterface $field) {
    foreach ($this->schema($field) as $name => $table) {
      db_create_table($name, $table);
    }
    drupal_get_schema(NULL, TRUE);
  }

  /**
   * Implements FieldStorageInterface::updateField().
   */
  public function updateField(FieldInterface $field, FieldInterface $prior_field, $has_data) {
    if (!$has_data) {
      // There is no data. Re-create the tables completely.

      if (Database::getConnection()->supportsTransactionalDDL()) {
        // If the database supports transactional DDL, we can go ahead and rely
        // on it. If not, we will have to rollback manually if something fails.
        $transaction = db_transaction();
      }

      try {
        $prior_schema = $this->schema($prior_field);
        foreach ($prior_schema as $name => $table) {
          db_drop_table($name, $table);
        }
        $schema = $this->schema($field);
        foreach ($schema as $name => $table) {
          db_create_table($name, $table);
        }
      }
      catch (Exception $e) {
        if (Database::getConnection()->supportsTransactionalDDL()) {
          $transaction->rollback();
        }
        else {
          // Recreate tables.
          $prior_schema = $this->schema($prior_field);
          foreach ($prior_schema as $name => $table) {
            if (!db_table_exists($name)) {
              db_create_table($name, $table);
            }
          }
        }
        throw $e;
      }
    }
    else {
      // There is data, so there are no column changes. Drop all the
      // prior indexes and create all the new ones, except for all the
      // priors that exist unchanged.

      $field_name = $field->name;
      $field_schema = $field->schema();
      $field_indexes = $field_schema['indexes'];
      $prior_field_schema = $prior_field->schema();
      $prior_field_indexes = $prior_field_schema['indexes'];

      // Drop the indexes that changed or disappeared.
      $table = $this->tableName($prior_field, FIELD_LOAD_CURRENT);
      $revision_table = $this->tableName($prior_field, FIELD_LOAD_REVISION);
      foreach ($prior_field_indexes as $name => $columns) {
        if (!isset($field_indexes[$name]) || $field_indexes[$name] != $prior_field_indexes[$name]) {
          $real_name = $this->indexName($prior_field, $name);

          db_drop_index($table, $real_name);
          db_drop_index($revision_table, $real_name);
        }
      }

      // Create the indexes that changed or appeared.
      $table = $this->tableName($field, FIELD_LOAD_CURRENT);
      $revision_table = $this->tableName($field, FIELD_LOAD_REVISION);
      foreach ($field_indexes as $name => $columns) {
        if (!isset($field_indexes[$name]) || $field_indexes[$name] != $prior_field_indexes[$name]) {
          $real_name = $this->indexName($field, $name);
          $real_columns = array();
          foreach ($columns as $column) {
            $real_columns[] = $this->columnName($field, $column);
          }

          db_add_index($table, $real_name, $real_columns);
          db_add_index($revision_table, $real_name, $real_columns);
        }
      }
    }

    drupal_get_schema(NULL, TRUE);
  }

  /**
   * Return the database schema for a field. This may contain one or
   * more tables. Each table will contain the columns relevant for the
   * specified field. Leave the $field's 'columns' and 'indexes' keys
   * empty to get only the base schema.
   *
   * @param FieldInterface $field
   *   The field structure for which to generate a database schema.
   * @return
   *   One or more tables representing the schema for the field.
   */
  protected function schema(FieldInterface $field) {
    $field_id = $field->id();
    $field_name = $field->name;
    $deleted = $field->deleted ? 'deleted ' : '';

    $current = array(
      'description' => "Data storage for {$deleted}field $field_id ($field_name)",
      'fields' => array(
        'entity_type' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The entity type this data is attached to',
        ),
        'bundle' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The field instance bundle to which this row belongs, used when deleting a field instance',
        ),
        'deleted' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'A boolean indicating whether this data item has been deleted'
        ),
        'entity_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'The entity id this data is attached to',
        ),
        'revision_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
          'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        ),
        'langcode' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The language code for this data item.',
        ),
        'delta' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'The sequence number for this data item, used for multi-value fields',
        ),
      ),
      'primary key' => array('entity_type', 'entity_id', 'deleted', 'delta', 'langcode'),
      'indexes' => array(
        'entity_type' => array('entity_type'),
        'bundle' => array('bundle'),
        'deleted' => array('deleted'),
        'entity_id' => array('entity_id'),
        'revision_id' => array('revision_id'),
        'langcode' => array('langcode'),
      ),
    );

    $schema = $field->schema();

    // Add field columns.
    foreach ($schema['columns'] as $column_name => $attributes) {
      $real_name =  $this->columnName($field, $column_name);
      $current['fields'][$real_name] = $attributes;
    }

    // Add indexes.
    foreach ($schema['indexes'] as $index_name => $columns) {
      $real_name = _field_sql_storage_indexname($field_name, $index_name);
      foreach ($columns as $column_name) {
        $current['indexes'][$real_name][] =  $this->columnName($field, $column_name);
      }
    }

    // Add foreign keys.
    foreach ($schema['foreign keys'] as $specifier => $specification) {
      $real_name = $this->indexName($field, $specifier);
      $current['foreign keys'][$real_name]['table'] = $specification['table'];
      foreach ($specification['columns'] as $column => $referenced) {
        $sql_storage_column = $this->columnName($field, $column_name);
        $current['foreign keys'][$real_name]['columns'][$sql_storage_column] = $referenced;
      }
    }

    // Construct the revision table.
    $revision = $current;
    $revision['description'] = "Revision archive storage for {$deleted}field $field_id ($field_name)";
    $revision['primary key'] = array('entity_type', 'entity_id', 'revision_id', 'deleted', 'delta', 'langcode');
    $revision['fields']['revision_id']['not null'] = TRUE;
    $revision['fields']['revision_id']['description'] = 'The entity revision id this data is attached to';

    return array(
      $this->tableName($field, FIELD_LOAD_CURRENT) => $current,
      $this->tableName($field, FIELD_LOAD_REVISION) => $revision,
    );
  }

  /**
   * Generate a table name for a field data table.
   *
   * @param FieldInterface $field
   *   The field.
   * @param $age
   *   Either FIELD_LOAD_CURRENT or FIELD_LOAD_REVISION.
   *
   * @return
   *   A string containing the generated name for the database table
   */
  protected function tableName(FieldInterface $field, $age) {
    switch ($age) {
      case FIELD_LOAD_CURRENT:
        if ($field->deleted) {
          $table_name = 'field_deleted_data_' . $field->id();
        }
        else {
          $table_name = 'field_data_' . $field->name;
        }
        break;

      case FIELD_LOAD_REVISION:
        if ($field->deleted) {
          $table_name = 'field_deleted_revision_' . $field->id();
        }
        else {
          $table_name = 'field_revision_' . $field->name;
        }
        break;
    }

    return $table_name;
  }

  /**
   * Generate a column name for a field data table.
   *
   * @param FieldInterface $field
   *   The field.
   * @param $column
   *   The name of the column.
   *
   * @return
   *   A string containing a generated column name for a field data table that
   *   is unique among all other fields.
   */
  protected function columnName(FieldInterface $field, $column) {
    return $field->name . '_' . $column;
  }

  /**
   * Generate an index name for a field data table.
   *
   * @param FieldInterface $field
   *   The field.
   * @param $index
   *   The name of the index.
   *
   * @return
   *   A string containing a generated index name for a field data table that is
   *   unique among all other fields.
   */
  protected function indexName(FieldInterface $field, $index) {
    return $field->name . '_' . $index;
  }
}
