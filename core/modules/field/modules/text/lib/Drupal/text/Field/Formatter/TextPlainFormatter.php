<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Formatter\TextPlainFormatter.
 */

namespace Drupal\text\Field\Formatter;
use Drupal\field\Field\Formatter\BaseFormatter;
use Drupal\entity\EntityInterface;

/**
 * Plugin implementation of the 'text_plain' formatter.
 */
class TextPlainFormatter extends BaseFormatter {

  /**
   * Implements FormatterInterface::viewElements().
   */
  public function viewElements($entity_type, EntityInterface $entity, $langcode, array $items) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = array('#markup' => strip_tags($item['value']));
    }

    return $elements;
  }
}
