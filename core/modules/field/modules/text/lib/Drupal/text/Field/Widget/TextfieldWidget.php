<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Widget\TextfieldWidget.
 */

namespace Drupal\text\Field\Widget;
use Drupal\field\Field\Widget\BaseWidget;

/**
 * Plugin implementation of the 'text_textfield' widget.
 */
class TextfieldWidget extends BaseWidget {

  /**
   * Implements WidgetInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['size'] = array(
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $main_widget = $element + array(
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
      '#size' => $this->getSetting('size'),
      '#maxlength' => $this->field->getSetting('max_length'),
      '#attributes' => array('class' => array('text-full')),
    );

    if ($this->instance->getSetting('text_processing')) {
      $element = $main_widget;
      $element['#type'] = 'text_format';
      $element['#format'] = isset($items[$delta]['format']) ? $items[$delta]['format'] : NULL;
      $element['#base_type'] = $main_widget['#type'];
    }
    else {
      $element['value'] = $main_widget;
    }

    return $element;
  }
}
