<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\FieldType\TextLongFieldHandler.
 */

namespace Drupal\text\Field\FieldType;
use Drupal\field\Field\FieldInstanceInterface;

/**
 * Plugin implementation of the 'text_long' field type.
 */
class TextLongFieldHandler extends BaseTextFieldHandler {

  /**
   * Implements FieldHandlerInterface::schema().
   * @todo : actually call.
   */
  public function schema() {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'indexes' => array(
        'format' => array('format'),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array('format' => 'format'),
        ),
      ),
    );
  }

  /**
   * Implements FieldHandlerInterface::instanceSettingsForm().
   */
  public function instanceSettingsForm(FieldInstanceInterface $instance, array $form, array &$form_state) {
    $element = array();

    $element['text_processing'] = array(
      '#type' => 'radios',
      '#title' => t('Text processing'),
      '#default_value' => $instance->getSetting('text_processing'),
      '#options' => array(
        t('Plain text'),
        t('Filtered text (user selects text format)'),
      ),
    );

    return $element;
  }
}
