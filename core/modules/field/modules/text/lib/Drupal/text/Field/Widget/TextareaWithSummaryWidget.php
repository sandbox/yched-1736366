<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Widget\TextareaWithSummaryWidget.
 */

namespace Drupal\text\Field\Widget;

/**
 * Plugin implementation of the 'text_textarea_with_summary' widget.
 */
class TextareaWithSummaryWidget extends TextareaWidget {

  /**
   * Overrides TextareaWidget::formElement().
   */
  function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $element = parent::formElement($items, $delta, $element, $langcode, $form, $form_state);

    $display_summary = !empty($items[$delta]['summary']) || $this->instance->getSetting('display_summary');
    $element['summary'] = array(
      '#type' => $display_summary ? 'textarea' : 'value',
      '#default_value' => isset($items[$delta]['summary']) ? $items[$delta]['summary'] : NULL,
      '#title' => t('Summary'),
      '#rows' => $this->getSetting('summary_rows'),
      '#description' => t('Leave blank to use trimmed value of full text as the summary.'),
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'text') . '/text.js'),
      ),
      '#attributes' => array('class' => array('text-summary')),
      '#prefix' => '<div class="text-summary-wrapper">',
      '#suffix' => '</div>',
      '#weight' => -10,
    );

    return $element;
  }

  /**
   * Overrides TextareaWidget::errorElement().
   */
  public function errorElement(array $element, array $error, array $form, array &$form_state) {
    switch ($error['error']) {
      case 'text_summary_max_length':
        $error_element = $element['summary'];
        break;

      default:
        $error_element = $element;
        break;
    }

    return $error_element;
  }
}
