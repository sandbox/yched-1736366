<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Formatter\TextTrimmedFormatter.
 */

namespace Drupal\text\Field\Formatter;
use Drupal\field\Field\Formatter\BaseFormatter;
use Drupal\entity\EntityInterface;

/**
 * Plugin implementation of the 'text_trimmed' and 'text_summary_or_trimmed' formatters.
 */
class TextTrimmedFormatter extends BaseFormatter {

  /**
   * Implements FormatterInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['trim_length'] = array(
      '#title' => t('Trim length'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('trim_length'),
      '#min' => 1,
      '#required' => TRUE,
    );

    return $element;
  }

  /**
   * Implements FormatterInterface::settingsForm().
   */
  public function settingsSummary() {
    return t('Trim length') . ': ' . $this->getSetting('trim_length');
  }

  /**
   * Implements FormatterInterface::viewElements().
   */
  public function viewElements($entity_type, EntityInterface $entity, $langcode, array $items) {
    $elements = array();

    foreach ($items as $delta => $item) {
      if ($this->getPluginId() == 'text_summary_or_trimmed' && !empty($item['summary'])) {
        $output = _text_sanitize($this->instance, $langcode, $item, 'summary');
      }
      else {
        $output = _text_sanitize($this->instance, $langcode, $item, 'value');
        $output = text_summary($output, $this->instance->getSetting('text_processing') ? $item['format'] : NULL, $this->getSetting('trim_length'));
      }
      $elements[$delta] = array('#markup' => $output);
    }

    return $elements;
  }
}
