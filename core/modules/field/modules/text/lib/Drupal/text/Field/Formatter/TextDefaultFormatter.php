<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Formatter\TextDefaultFormatter.
 */

namespace Drupal\text\Field\Formatter;
use Drupal\field\Field\Formatter\BaseFormatter;
use Drupal\entity\EntityInterface;

/**
 * Plugin implementation of the 'text_default' formatter.
 */
class TextDefaultFormatter extends BaseFormatter {

  /**
   * Implements FormatterInterface::viewElements().
   */
  public function viewElements($entity_type, EntityInterface $entity, $langcode, array $items) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $output = _text_sanitize($this->instance, $langcode, $item, 'value');
      $elements[$delta] = array('#markup' => $output);
    }

    return $elements;
  }
}
