<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\FieldType\TextFieldHandler.
 */

namespace Drupal\text\Field\FieldType;
use Drupal\field\Field\FieldInstanceInterface;

/**
 * Plugin implementation of the 'text' field type.
 */
class TextFieldHandler extends BaseTextFieldHandler {

  /**
   * Implements FieldHandlerInterface::schema().
   * @todo: recheck why hook_field_schema() was moved to .install in D7.
   */
  public function schema() {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => $this->field->getSetting('max_length'),
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ),
      ),
      'indexes' => array(
        'format' => array('format'),
      ),
      'foreign keys' => array(
        'format' => array(
          'table' => 'filter_format',
          'columns' => array('format' => 'format'),
        ),
      ),
    );
  }

  /**
   * Implements FieldHandlerInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['max_length'] = array(
      '#type' => 'number',
      '#title' => t('Maximum length'),
      '#default_value' => $this->field->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the field in characters.'),
      '#min' => 1,
      // @todo: If $has_data, add a validate handler that only allows
      // max_length to increase.
      '#disabled' => $this->field->hasData(),
    );

    return $element;
  }

  /**
   * Implements FieldHandlerInterface::instanceSettingsForm().
   */
  public function instanceSettingsForm(FieldInstanceInterface $instance, array $form, array &$form_state) {
    $element = array();

    $element['text_processing'] = array(
      '#type' => 'radios',
      '#title' => t('Text processing'),
      '#default_value' => $instance->getSetting('text_processing'),
      '#options' => array(
        t('Plain text'),
        t('Filtered text (user selects text format)'),
      ),
    );

    return $element;
  }
}
