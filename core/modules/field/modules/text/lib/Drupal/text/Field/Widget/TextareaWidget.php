<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\Widget\TextareaWidget.
 */

namespace Drupal\text\Field\Widget;
use Drupal\field\Field\Widget\BaseWidget;

/**
 * Plugin implementation of the 'text_textarea' widget.
 */
class TextareaWidget extends BaseWidget {

  /**
   * Implements WidgetInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['rows'] = array(
      '#type' => 'number',
      '#title' => t('Rows'),
      '#default_value' => $this->getSetting('rows'),
      '#required' => TRUE,
      '#min' => 1,
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $main_widget = $element + array(
      '#type' => 'textarea',
      '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
      '#rows' => $this->getSetting('rows'),
      '#attributes' => array('class' => array('text-full')),
    );

    if ($this->instance->getSetting('text_processing')) {
      $element = $main_widget;
      $element['#type'] = 'text_format';
      $element['#format'] = isset($items[$delta]['format']) ? $items[$delta]['format'] : NULL;
      $element['#base_type'] = $main_widget['#type'];
    }
    else {
      $element['value'] = $main_widget;
    }

    return $element;
  }
}
