<?php

/**
 * @file
 *
 * Definition of Drupal\text\Field\FieldType\BaseTextFieldHandler.
 */

namespace Drupal\text\Field\FieldType;
use Drupal\field\Field\FieldType\BaseFieldHandler;
use Drupal\field\Field\FieldInstanceInterface;
use Drupal\entity\EntityInterface;

/**
 * Base class for 'text' field types.
 */
abstract class BaseTextFieldHandler extends BaseFieldHandler {

  /*
   * Implements FieldInterface::itemIsEmpty().
   */
  public function itemIsEmpty(array $item) {
    return ($item['value'] === '');
  }

  /**
   * Overrides FieldHandler::load().
   *
   * Where possible, generate the sanitized version of each field early so that
   * it is cached in the field cache. This avoids looking up from the filter
   * cache separately.
   */
  public function load($entity_type, array $entities, array $instances, $langcode, array &$items, $age) {
    parent::load($entity_type, $entities, $instances, $langcode, $items, $age);

    foreach ($entities as $id => $entity) {
      foreach ($items[$id] as $delta => &$item) {
        // Only process items with a cacheable format, the rest will be handled
        // by formatters if needed.
        $instance = $instances[$id];
        if (!$instance->getSetting('text_processing') || filter_format_allowcache($item['format'])) {
          $item['safe_value'] = isset($item['value']) ? _text_sanitize($instance, $langcode, $item, 'value') : '';
          if ($this->getPluginId() == 'text_with_summary') {
            $item['safe_summary'] = isset($item['summary']) ? _text_sanitize($instance, $langcode, $item, 'summary') : '';
          }
        }
      }
    }
  }

  /**
   * Overrides FieldHandler::validate().
   */
  public function validate($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array $items, array &$errors) {
    parent::validate($entity_type, $entity, $instance, $langcode, $items, $errors);

    $field_name = $this->field->name;
    $max_length = $this->field->getSetting('max_length');

    foreach ($items as $delta => $item) {
      // Just to quickly test validation errors.
      // @todo remove
      if ($item['value'] == 'aa') {
        $errors[$field_name][$langcode][$delta][] = array(
          'error' => "text_dummy_error",
          'message' => 'aa is invalid, dude',
        );
      }

      // @todo Length is counted separately for summary and value, so the maximum
      //   length can be exceeded very easily.
      foreach (array('value', 'summary') as $column) {
        if (!empty($item[$column])) {
          if ($max_length && drupal_strlen($item[$column]) > $max_length) {
            switch ($column) {
              case 'value':
                $message = t('%name: the text may not be longer than %max characters.', array('%name' => $instance->label, '%max' => $max_length));
                break;

              case 'summary':
                $message = t('%name: the summary may not be longer than %max characters.', array('%name' => $instance->label, '%max' => $max_length));
                break;
            }
            $errors[$field_name][$langcode][$delta][] = array(
              'error' => "text_{$column}_length",
              'message' => $message,
            );
          }
        }
      }
    }
  }

  /**
   * Overrides FieldHandler::prepareTranslation().
   */
  public function prepareTranslation($entity_type, EntityInterface $entity, FieldInstanceInterface $instance, $langcode, array &$items, $source_entity, $source_langcode) {
    parent::prepareTranslation($entity_type, $entity, $instance, $langcode, $items, $source_entity, $source_langcode);

    $field_name = $this->field->name;

    // If the translating user is not permitted to use the assigned text format,
    // we must not expose the source values.
    if (!empty($source_entity->{$field_name}[$source_langcode])) {
      $formats = filter_formats();
      foreach ($source_entity->{$field_name}[$source_langcode] as $delta => $item) {
        $format_id = $item['format'];
        if (!empty($format_id) && !filter_access($formats[$format_id])) {
          unset($items[$delta]);
        }
      }
    }
  }

  // @todo Just for testing - remove...
  public function prepareView($entity_type, array $entities, array $instances, $langcode, array &$items, $display) {
    foreach ($entities as $id => $entity) {
      foreach ($items[$id] as $delta => $item) {
        $items[$id][$delta]['safe_value'] = $items[$id][$delta]['safe_value'];
      }
    }

    // Let the parent call the formatter's prepareView().
    parent::prepareView($entity_type, $entities, $instances, $langcode, $items, $display);
  }
}
