<?php

/**
 * @file
 *
 * Definition of Drupal\field_test\Field\Widget\TestWidget.
 */

namespace Drupal\field_test\Field\Widget;
use Drupal\field\Field\Widget\BaseWidget;

/**
 * Plugin implementation of the 'test_widget' widget.
 */
class TestWidget extends BaseWidget {

  /**
   * Implements WidgetInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['test_widget_setting'] = array(
      '#type' => 'textfield',
      '#title' => t('Field test field widget setting'),
      '#default_value' => $this->getSetting('test_widget_setting'),
      '#required' => FALSE,
      '#description' => t('A dummy form element to simulate field widget setting.'),
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $element += array(
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : '',
    );

    return array('value' => $element);
  }

  /**
   * Overrides WidgetBase::errorElement().
   */
  public function errorElement(array $element, array $error, array $form, array &$form_state) {
    return $element['value'];
  }
}
