<?php

/**
 * @file
 *
 * Definition of Drupal\field_test\Field\Storage\TestStorageFailure.
 */

namespace Drupal\field_test\Field\Storage;
use Drupal\field\Field\FieldInterface;

/**
 * Plugin implementation of the 'field_test_storage_failure' field storage type.
 */
class TestStorageFailure extends TestStorage {

  /**
   * Overrides TestStorageFailure::createField().
   */
  public function createField(FieldInterface $field) {
    throw new Exception('field_test_storage_failure engine always fails to create fields');
  }
}
