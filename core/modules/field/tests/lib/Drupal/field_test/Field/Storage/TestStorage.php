<?php

/**
 * @file
 *
 * Definition of Drupal\field_test\Field\Storage\TestStorage.
 */

namespace Drupal\field_test\Field\Storage;
use Drupal\field\Field\Storage\BaseFieldStorage;
use Drupal\field\Field\FieldInterface;

/**
 * Plugin implementation of the 'field_test_storage' field storage type.
 */
class TestStorage extends BaseFieldStorage {

  /**
   * Implements FieldStorageInterface::getDetails().
   */
  function getDetails(FieldInterface $field) {
    $details = array();

    // Add field columns.
    $columns = array();
    foreach ((array) $field['columns'] as $column_name => $attributes) {
      $columns[$column_name] = $column_name;
    }
    return array(
      'drupal_variables' => array(
        'field_test_storage_data[FIELD_LOAD_CURRENT]' => $columns,
        'field_test_storage_data[FIELD_LOAD_REVISION]' => $columns,
      ),
    );
  }

  /**
   * Implements FieldStorageInterface::createField().
   */
  public function createField(FieldInterface $field) {
    $data = $this->getData();

    $id = $field->id();
    $data[$id] = array(
      'current' => array(),
      'revisions' => array(),
    );

    $this->setData($data);
  }

  /**
   * Returns the current state of the data store.
   *
   * @return
   *   The array of stored data.
   */
  protected function getData() {
    return variable_get('field_test_storage_data', array());
  }

  /**
   * Sets the state of the data store.
   *
   * @param array $data
   *   The data to store.
   */
  protected function setData(array $data) {
    variable_set('field_test_storage_data', $data);
  }

}
