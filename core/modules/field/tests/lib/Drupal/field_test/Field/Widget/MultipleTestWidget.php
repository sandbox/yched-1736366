<?php

/**
 * @file
 *
 * Definition of Drupal\field_test\Field\Widget\MultipleTestWidget.
 */

namespace Drupal\field_test\Field\Widget;
use Drupal\field\Field\Widget\BaseWidget;

/**
 * Plugin implementation of the 'multiple_test_widget' widget.
 */
class MultipleTestWidget extends BaseWidget {

  /**
   * Implements WidgetInterface::settingsForm().
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = array();

    $element['test_widget_setting_multiple'] = array(
      '#type' => 'textfield',
      '#title' => t('Field test field widget setting'),
      '#default_value' => $this->getSetting('test_widget_setting_multiple'),
      '#required' => FALSE,
      '#description' => t('A dummy form element to simulate field widget setting.'),
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::formElement().
   */
  public function formElement(array $items, $delta, array $element, $langcode, array &$form, array &$form_state) {
    $values = array();
    foreach ($items as $delta => $value) {
      $values[] = $value['value'];
    }
    $element += array(
      '#type' => 'textfield',
      '#default_value' => implode(', ', $values),
      '#element_validate' => array('field_test_multiple_test_widget_validate'),
    );

    return $element;
  }

  /**
   * Implements WidgetInterface::fieldValuesFromFormValues().
   */
  public function fieldValuesFromFormValues($values, $form, &$form_state) {
    $values = array_map('trim', explode(',', $values));
    $items = array();
    foreach ($values as $value) {
      $items[] = array('value' => $value);
    }
    return $items;
  }

  /**
   * Overrides WidgetBase::errorElement().
   */
  public function errorElement(array $element, array $error, array $form, array &$form_state) {
    return $element['value'];
  }
}
